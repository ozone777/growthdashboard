task :growth_to_server, :roles => [:web] do

  #run "cd #{release_path} && rm -rf public/.htaccess"
  #run "cd #{release_path} && rm -rf config/s3_credentials.yml"
  #run "cd #{release_path} && rm -rf vendor/bundle"
  #run "cd #{release_path} && rm -rf config/database.yml"
  #run "cd #{release_path} && rm -rf config/environments/production.rb"
  #run "cd #{release_path} && rm -rf config/initializers/omniauth.rb"
  
  #RAILS_ENV=production script/delayed_job start    

  #run "ln -s /home/ozone/#{application}/shared/bundle #{release_path}/vendor/bundle"  
  
  #run "ln -s #{shared_path}/production.rb #{release_path}/config/environments/production.rb" 
  #run "ln -s #{shared_path}/omniauth.rb #{release_path}/config/initializers/omniauth.rb" 
  
                                                                                                               
  #run "cd #{release_path} && bundle install --deployment"
  #run "cd #{release_path} && bundle exec rake db:migrate RAILS_ENV=production"
  #run "cd #{release_path} && bundle exec rake assets:precompile --trace"
  
  #run "cd #{release_path} && rm -rf .git"

  
                                                                                                                                                                                                                                
                                                                                                   
  #run "chown -R ozone:wheel #{release_path}/tmp"                                                                              
  #run "chown -R ozone:wheel #{release_path}/log" 
  #run "cd #{release_path} && bundle exec rake admin:initialize_template RAILS_ENV=production"
  #run "/etc/init.d/nginx restart"
  #run "/etc/init.d/httpd restart"  
  #run "systemctl restart nginx.service" 
  
  
  #run "cd #{release_path} && rm -rf config/database.php"
  #run "ln -s #{shared_path}/database.php #{release_path}/config/database.php" 
  
  run "cd #{release_path} && rm -rf .env"
  run "ln -s #{shared_path}/.env #{release_path}/.env" 
  
  run "chown -R apache:apache #{shared_path}"                                                                                                  
  run "chown -R apache:apache #{release_path}"
  
  run "cd #{release_path} && composer install"
  #run "cd #{release_path} && php artisan migrate"
  
  run "sudo /bin/systemctl reload httpd.service"
  

  
                                                                                     
end




task :growth do
  ssh_options[:port] = 13727
  set :ip, "96.126.122.204" 
  set :user, "root" 
  #nombre del dominio
  set :server_name, "96.126.122.204" 
  #La direccion en donde el repositorio
  set :domain, "ozone.sourcerepo.com" 
  #nombre de la aplicacion
  set :application, "growthdashboard"
  set :default_shell, '/bin/bash -l'
  set :scm, :git 
  #Nombre del repositorio
  set :repository, "git@#{domain}:ozone/#{application}.git" 
  #Donde yo quiero que lo grabe en linux
  set :deploy_to, "/var/www/#{application}" 
  set :use_sudo, false 
  set :group_writable, false
  set :branch, 'ozonevolution2' 
  set :deploy_via, :checkout 
  default_run_options[:pty] = true 
  set :rails_env, "production" 
  role :app, server_name 
  role :web, server_name 
  role :db, server_name, :primary => true
end




#Paso 1
#Setting automatic
#cap template18:parameter -s cache=TEMPLATE18 -s database=teachsta_template18

#Paso amazon integration
#rake admin:

#Paso 2 
#Cuadrar base de datos
#CREATE USER 'teachstarsuser'@'104.237.135.120' IDENTIFIED BY 'user2expert777.';

#GRANT ALL PRIVILEGES ON teachsta_conuespi.* TO 'teachstarsuser'@'104.237.135.120'; OJO: verificar la ip del usuario de la base de datos...no siempre está en el mismo servidor.


#Paso 3
#cuadrar Archivos delicados
#mkdir bundle
#database.yml
#production.rb

#Paso 4
#Hacer deploy 
#cap cofca_staging

#Paso 5

#/opt/ngins/conf
#server {
#        listen       80;
#        client_max_body_size 20M;
#        server_name  figi.teachstars.com;

#        location / {
#            root   /home/ozone/cofca_staging/current/public;
#            passenger_enabled on;
#        }
#    }


#after :growth, 'deploy:setup'
#after :template1, :deploy
#after :cofca_staging, 'deploy:setup'

after 'deploy:finalize_update', :growth_to_server
after :growth, :deploy


