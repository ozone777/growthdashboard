<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wp_omission extends Model
{
    //
	 protected $fillable = array('completed', 'accepted', 'in_progress','suspended','aborted','ready_for_revision','keywords','examples','guidelines','blog_title','blog_goal_improve_seo','blog_goal_become_a_thought_leader','blog_goal_recycle_existing_content','blog_goal_provide_instructions','blog_tone_serious_experienced_formal','blog_tone_funny_witty_satirical','blog_tone_empathetic_instructional_detailed','blog_tone_casual_conversational_friendly','color1','color2','color3','color4','color5','web_design_admin_email','web_design_admin_username','web_design_admin_desired_password','web_design_admin_site_name','web_design_domain','web_design_logo','web_design_logo','web_design_section1_title','web_design_section2_title','web_design_section3_title','web_design_section4_title','web_design_section5_title','web_design_section6_title','web_design_section7_title','web_design_section1_content','web_design_section2_content','web_design_section3_content','web_design_section4_content','web_design_section5_content','web_design_section6_content','web_design_section7_content','web_design_contact_form_email','web_design_social_urls','web_design_domain_user_and_password','store_types_of_products','store_product_fields_size','store_product_fields_color','store_product_fields_price','store_product_fields_weight','store_product_fields_images','store_currency','store_delivery_options','store_delivery_options','store_delivery_areas','store_location','store_product_checkout_yes','store_product_checkout_no','landing_lead_mailchimp','landing_lead_csv','landing_lead_email','landing_lead_admin_email','landing_main_cta','social_media_users_and_passwords','infographic_title','infographic_content');
	 	
}
