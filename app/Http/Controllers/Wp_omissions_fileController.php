<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Wp_omissions_file;
use App\Wp_omissions_style;
use Log;
use Illuminate\Http\Request;

use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Orchestra\Imagine\Facade as Imagine;

class Wp_omissions_fileController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$wp_omissions_files = Wp_omissions_file::orderBy('id', 'desc')->paginate(10);
		$viewsw = "my_missions";
		return view('wp_omissions_files.index', compact('wp_omissions_files','viewsw'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{	$viewsw = "my_missions";
		return view('wp_omissions_files.create',compact('viewsw'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$wp_omissions_file = new Wp_omissions_file();

		$wp_omissions_file->title = $request->input("title");
        $wp_omissions_file->section = $request->input("section");
		$wp_omissions_file->omission_id = $request->input("omission_id");
		$wp_omissions_file->updated_view = $request->input("updated_view");
		
		$updated_view = $request->input("updated_view");
			
		if($request->file('file')!= ""){

			Log::info('Entro en guardar archivo');
			$file = $request->file('file');
	        $destinationPath = 'files/'.$request->input("omission_id");
	        $extension = $file->getClientOriginalExtension();
			$fileName_solo = md5(uniqid(rand(), true));
	        $fileName = $fileName_solo . '.' . $extension;
	        $upload_success = $file->move($destinationPath, $fileName);
			
			$wp_omissions_file->file = $fileName_solo;
			$wp_omissions_file->extension = $extension;
			
			Wp_omissions_file::create_thumbnail($destinationPath, $fileName_solo, $extension);
			
			if($wp_omissions_file->section == "mockup"){
				Wp_omissions_file::create_mockup($destinationPath, $fileName_solo, $extension);
			}
			
		}

		$wp_omissions_file->save();
		
		return redirect()->action('Wp_omissionController@edit', ['id' => $wp_omissions_file->omission_id,'updated_view' => $updated_view]);
		//return redirect()->route('wp_omissions.edit', $wp_omissions_file->omission_id)->with('message', 'Item updated successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{	$viewsw = "my_missions";
		$wp_omissions_file = Wp_omissions_file::findOrFail($id);

		return view('wp_omissions_files.show', compact('wp_omissions_file','viewsw'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{	$viewsw = "my_missions";
		$wp_omissions_file = Wp_omissions_file::findOrFail($id);

		return view('wp_omissions_files.edit', compact('wp_omissions_file','viewsw'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$wp_omissions_file = Wp_omissions_file::findOrFail($id);

		$wp_omissions_file->title = $request->input("title");
        $wp_omissions_file->file = $request->input("file");
        $wp_omissions_file->section = $request->input("section");

		$wp_omissions_file->save();

		return redirect()->route('wp_omissions_files.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$wp_omissions_file = Wp_omissions_file::findOrFail($id);
		$wp_omissions_file->delete();

		return redirect()->route('wp_omissions_files.index')->with('message', 'Item deleted successfully.');
	}
	
	function create_thumbnail($path, $filename, $extension)
	{
	    $width  = 320;
	    $height = 320;
	    $mode   = ImageInterface::THUMBNAIL_OUTBOUND;
	    $size   = new Box($width, $height);

	    $thumbnail   = Imagine::open("{$path}/{$filename}.{$extension}")->thumbnail($size, $mode);
	    $destination = "{$filename}.thumb.{$extension}";

	    $thumbnail->save("{$path}/{$destination}");
	}

}
