<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Wp_omissions_style;
use Illuminate\Http\Request;

class Wp_omissions_styleController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$wp_omissions_styles = Wp_omissions_style::orderBy('id', 'desc')->paginate(30);
		$viewsw = "styles";
		return view('wp_omissions_styles.index', compact('wp_omissions_styles','viewsw'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{	$viewsw = "styles";
		return view('wp_omissions_styles.create',compact('viewsw'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$wp_omissions_style = new Wp_omissions_style();

		$wp_omissions_style->es = $request->input("es");
        $wp_omissions_style->en = $request->input("en");
       
        $wp_omissions_style->description_en = $request->input("description_en");
		$wp_omissions_style->description_es = $request->input("description_es");
		
	   if($request->file("file")!= ""){
		
		$file = $request->file("file");
        // SET UPLOAD PATH
        $destinationPath = 'styles';
         // GET THE FILE EXTENSION
        $extension = $file->getClientOriginalExtension();
         // RENAME THE UPLOAD WITH RANDOM NUMBER
        $fileName = rand(11111, 99999) . '.' . $extension;
         // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY
        $upload_success = $file->move($destinationPath, $fileName);
		$wp_omissions_style->file = $fileName;
		
	   }

		$wp_omissions_style->save();

		return redirect()->route('wp_omissions_styles.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$wp_omissions_style = Wp_omissions_style::findOrFail($id);
		$viewsw = "styles";
		return view('wp_omissions_styles.show', compact('wp_omissions_style','viewsw'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$wp_omissions_style = Wp_omissions_style::findOrFail($id);
		$viewsw = "styles";
		return view('wp_omissions_styles.edit', compact('wp_omissions_style','viewsw'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$wp_omissions_style = Wp_omissions_style::findOrFail($id);

		$wp_omissions_style->es = $request->input("es");
        $wp_omissions_style->en = $request->input("en");
       
        $wp_omissions_style->description_en = $request->input("description_en");
		$wp_omissions_style->description_es = $request->input("description_es");
		
 	   if($request->file("file")!= ""){
		
 		$file = $request->file("file");
         // SET UPLOAD PATH
         $destinationPath = 'styles';
          // GET THE FILE EXTENSION
         $extension = $file->getClientOriginalExtension();
          // RENAME THE UPLOAD WITH RANDOM NUMBER
         $fileName = rand(11111, 99999) . '.' . $extension;
          // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY
         $upload_success = $file->move($destinationPath, $fileName);
 		$wp_omissions_style->file = $fileName;
		
 	   }

		$wp_omissions_style->save();

		return redirect()->route('wp_omissions_styles.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$wp_omissions_style = Wp_omissions_style::findOrFail($id);
		$wp_omissions_style->delete();

		return redirect()->route('wp_omissions_styles.index')->with('message', 'Item deleted successfully.');
	}

}
