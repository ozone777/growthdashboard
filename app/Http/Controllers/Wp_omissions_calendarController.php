<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Wp_omissions_calendar;
use Illuminate\Http\Request;

class Wp_omissions_calendarController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$wp_omissions_calendars = Wp_omissions_calendar::orderBy('id', 'desc')->paginate(10);

		return view('wp_omissions_calendars.index', compact('wp_omissions_calendars'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('wp_omissions_calendars.create');
		
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$wp_omissions_calendar = new Wp_omissions_calendar();

		$wp_omissions_calendar->start_date = $request->input("start_date");
        $wp_omissions_calendar->social_network = $request->input("social_network");
        $wp_omissions_calendar->content = $request->input("content");
        $wp_omissions_calendar->link = $request->input("link");
        $wp_omissions_calendar->file = $request->input("file");
        $wp_omissions_calendar->omission_id = $request->input("omission_id");
		
		

		$wp_omissions_calendar->save();
		
		return redirect()->route('wp_omissions_calendars.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$wp_omissions_calendar = Wp_omissions_calendar::findOrFail($id);

		return view('wp_omissions_calendars.show', compact('wp_omissions_calendar'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$wp_omissions_calendar = Wp_omissions_calendar::findOrFail($id);

		return view('wp_omissions_calendars.edit', compact('wp_omissions_calendar'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$wp_omissions_calendar = Wp_omissions_calendar::findOrFail($id);

		$wp_omissions_calendar->start_date = $request->input("start_date");
        $wp_omissions_calendar->social_network = $request->input("social_network");
        $wp_omissions_calendar->content = $request->input("content");
        $wp_omissions_calendar->link = $request->input("link");
       // $wp_omissions_calendar->omission_id = $request->input("omission_id");
		
		if($request->file('file')!= ""){
			
			$file = $request->file('file');
	        // SET UPLOAD PATH
	        $destinationPath = 'ocalendars';
	         // GET THE FILE EXTENSION
	        $extension = $file->getClientOriginalExtension();
	         // RENAME THE UPLOAD WITH RANDOM NUMBER
	        $fileName = rand(11111, 99999) . '.' . $extension;
	         // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY
	        $upload_success = $file->move($destinationPath, $fileName);
			$wp_omissions_calendar->file = $fileName;
		}
		$viewsw = "my_missions";
		$wp_omissions_calendar->save();
		
		return redirect()->route('wp_omissions.edit', $wp_omissions_calendar->omission_id)->with('message', 'Item updated successfully.');

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$wp_omissions_calendar = Wp_omissions_calendar::findOrFail($id);
		$wp_omissions_calendar->delete();

		return redirect()->route('wp_omissions_calendars.index')->with('message', 'Item deleted successfully.');
	}

}
