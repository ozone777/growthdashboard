<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App;
use App\Wp_omission;
use App\Wp_omissions_file;
use App\Wp_omissions_style;
use App\Wp_omissions_styles_mission;
use App\Wp_omissions_calendar;
use Illuminate\Http\Request;
use Log;
use DB;
use Input;

class Wp_omissionController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	
    public function __construct(){
        $this->middleware('auth.wp');
		
			
	}
	
	public function index()
	{
		
		
		$user = Auth::user();
		$role = getUserRole($user->ID);
		
		
		Log::info("hola role: $role");
		
		$viewsw = "my_missions";
		$items = DB::select('SELECT p.ID as order_id, i.order_item_id as order_item_id, im.meta_value as product_id FROM wp_woocommerce_order_items i 
inner join wp_posts p on p.ID = i.order_id 
inner join wp_woocommerce_order_itemmeta im on im.order_item_id = i.order_item_id 
inner join wp_postmeta pm on p.ID = pm.post_id 
where im.meta_key = "_product_id" and pm.meta_key = "_customer_user" and pm.meta_value = "' . $user->ID .'" and p.post_parent = 0 Order by p.id DESC ') ;
		

		return view('wp_omissions.index', compact('wp_omissions','viewsw','items','role'));
		
	}
	
	public function get_calendars(){
		
		$social_network = input::get('social_network');
		$time = time();
		
		if(input::get('data') == "current_week"){

			$initial_date = date ('Y/m/d',strtotime("last Monday"));
			$last_date = date ( 'Y/m/d',strtotime("next Sunday"));
		
		}else if(input::get('data') == "last_week"){
			
			$initial_date = date('Y/m/d' ,strtotime('1 weeks ago'));
			$last_date = date('Y/m/d' ,strtotime('now'));
		}
		else if(input::get('data') == "current_month"){
			
			$initial_date = date('Y/m/d' ,strtotime(date('Y-m-1 00:00:00', $time)));
		    $last_date = date('Y/m/d',strtotime('+1 month -1 day', strtotime(date('Y-m-1 00:00:00', $time))));					
			
		}else if(input::get('data') == "next_week"){
			
			$initial_date = date ('Y/m/d',strtotime("next Monday"));
		    $last_date = date ('Y/m/d',strtotime("next Sunday"));
			
		}else if(input::get('data') == "next_month"){
	 
			$initial_date = date ( 'Y/m/d' ,strtotime('first day of +1 month'));
			$last_date = date ( 'Y/m/d' ,strtotime('last day of +1 month'));
			
		}else if(input::get('data') == "January"){
			
			$initial_date = date('Y/m/d',strtotime(date('Y-01-01')));
			$last_date = date('Y/m/d',strtotime(date('Y-01-31')));
			
		}else if(input::get('data') == "February"){
			
			$initial_date = date('Y/m/d',strtotime(date('Y-02-01')));
			$last_date = date('Y/m/d',strtotime(date('Y-02-31')));
			
		}else if(input::get('data') == "March"){
			
			$initial_date = date('Y/m/d',strtotime(date('Y-03-01')));
			$last_date = date('Y/m/d',strtotime(date('Y-03-31')));
			
		}else if(input::get('data') == "April"){
			
			$initial_date = date('Y/m/d',strtotime(date('Y-04-01')));
			$last_date = date('Y/m/d',strtotime(date('Y-04-31')));
			
		}else if(input::get('data') == "May"){
			
			$initial_date = date('Y/m/d',strtotime(date('Y-05-01')));
			$last_date = date('Y/m/d',strtotime(date('Y-05-31')));
			
		}else if(input::get('data') == "June"){
			
			$initial_date = date('Y/m/d',strtotime(date('Y-06-01')));
			$last_date = date('Y/m/d',strtotime(date('Y-06-31')));
			
		}else if(input::get('data') == "July"){
			
			$initial_date = date('Y/m/d',strtotime(date('Y-07-01')));
			$last_date = date('Y/m/d',strtotime(date('Y-07-31')));
			
		}else if(input::get('data') == "August"){
			
			$initial_date = date('Y/m/d',strtotime(date('Y-08-01')));
			$last_date = date('Y/m/d',strtotime(date('Y-08-31')));
			
		}else if(input::get('data') == "September"){
			
			$initial_date = date('Y/m/d',strtotime(date('Y-09-01')));
			$last_date = date('Y/m/d',strtotime(date('Y-09-31')));
			
		}else if(input::get('data') == "October"){
			
			$initial_date = date('Y/m/d',strtotime(date('Y-10-01')));
			$last_date = date('Y/m/d',strtotime(date('Y-10-31')));
			
		}else if(input::get('data') == "November"){
			
			$initial_date = date('Y/m/d',strtotime(date('Y-11-01')));
			$last_date = date('Y/m/d',strtotime(date('Y-11-31')));
			
		}else if(input::get('data') == "December"){
			
			$initial_date = date('Y/m/d',strtotime(date('Y-12-01')));
			$last_date = date('Y/m/d',strtotime(date('Y-12-31')));
			
		}
		
		
		$sql = "SELECT * FROM wp_omissions_calendars where social_network = '{$social_network}' and start_date >= '{$initial_date}' and start_date <= '{$last_date}'";
		$wp_omissions_calendars = DB::select($sql);
		
		//return response()->json(['name' => 'Abigail', 'state' => 'CA']);
		
		return response()->json(['calendars' => $wp_omissions_calendars, 'social_network' => $social_network]);
		
		
	}
	
	public function set_calendar()
	{
		
		$wp_omissions_calendar = Wp_omissions_calendar::findOrFail(input::get('calendar_id'));
		Log::info("hola bebo");
		
		if(input::get('db_field_name') == "content"){
		  $wp_omissions_calendar->content = input::get('data');
		}
		
		if(input::get('db_field_name') == "link"){
		  $wp_omissions_calendar->link = input::get('data');
		}
		
		if(input::get('db_field_name') == "start_date"){
		  $wp_omissions_calendar->start_date = input::get('data');
		}
		
		$wp_omissions_calendar->save();
		return response()->json($wp_omissions_calendar);
		
	}
	
	
	public function set_calendar_image(Request $request)
	{
		
		$wp_omissions_calendar = Wp_omissions_calendar::findOrFail(input::get('calendar_id'));
		
		$post_data = Input::all();
		Log::info($post_data);
		
		Log::info(input::get('calendar_id'));
		
		Log::info("voy a entrar al if");
		if ($request->file('ofile_name')) {
			Log::info("entro al if");
			$file = $request->file('ofile_name');
	        // SET UPLOAD PATH
	        $destinationPath = "ocalendars/{$wp_omissions_calendar->omission_id}";
	         // GET THE FILE EXTENSION
	        $extension = $file->getClientOriginalExtension();
	         // RENAME THE UPLOAD WITH RANDOM NUMBER
	        $fileName = md5(uniqid(rand(), true)) . '.' . $extension;
	         // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY
	        $upload_success = $file->move($destinationPath, $fileName);
			$wp_omissions_calendar->file = $fileName;
			$wp_omissions_calendar->save();
		}

		return response()->json($wp_omissions_calendar);
		//return response()->json($request->all());
		
	}
	
	public function delete_file($id)
	{
		$wp_omissions_file = Wp_omissions_file::findOrFail($id);
		$wp_omission = Wp_omission::findOrFail($wp_omissions_file->omission_id);
		$wp_omissions_file->delete();
		
		$id = $wp_omission->id;
		//return redirect("wp_omissions/edit/{$id}");
		return redirect()->action('Wp_omissionController@edit', compact('id'));
		
	}
	
	public function set_lang(){
		$user = Auth::user();
		
		#Output model
		Log::info('User: ' . $user->user_login);		
		DB::table('wp_users')->where('ID', $user->ID)->update(['ogrowthlang' => input::get('lang')]);
		
		return response()->json(['lang' => input::get('lang')]);
	}
	
	
	public function set_omission(){
		//$site = Site::withTrashed()->findOrFail(Input::get('id'));
		
		$viewsw = "my_missions";
		$order_id = Input::get('order_id');
		$order_item_id = Input::get('order_item_id');
		$user_id = Input::get('user_id');
		$product_id = Input::get('product_id');
		$message = "";
		
		$wp_omissions = DB::select('SELECT * FROM wp_omissions where order_id = "' . $order_id . '" and order_item_id = "' . $order_item_id . '" and user_id = "' . $user_id . '"');
		
		if(isset($wp_omissions[0])){
			$message = "Si existe";
			$wp_omission = Wp_omission::findOrFail($wp_omissions[0]->id);
			$wp_omission->product_id = $product_id;
		}else{
			$message = "No existia";
			$wp_omission = new Wp_omission();
			$wp_omission->order_id = $order_id;
			$wp_omission->order_item_id = $order_item_id;
			$wp_omission->user_id = $user_id;
			$wp_omission->product_id = $product_id;
			$wp_omission->save();
		}
		
		$id = $wp_omission->id;
		//$product = get_product($wp_omission->product_id);
		$updated_view = "index";
		//return redirect("wp_omissions/edit/{$id}");
		//return redirect()->action('Wp_omissionController@edit', compact('id','from'));
		return redirect()->action('Wp_omissionController@edit', ['id' => $wp_omission->id,'updated_view' => $updated_view]);
		//return view('wp_omissions.set_omission', compact('order_id','order_item_id','user_id','message'));
	}
	
	public function edit($id)
	{
		
	   $viewsw = "my_missions";
	   $updated_view = $_GET["updated_view"];
		   
	   
	   
	   $wp_omission = Wp_omission::findOrFail($id);
	   $product = get_product($wp_omission->product_id);
	   
	   if($product->get_title() == "Article Writing"){
		
		return redirect()->action('Wp_omissionController@article', compact('id'));
		
	   }else if(strpos($product->get_title(), 'Social') !== FALSE){
		   
		  return redirect()->action('Wp_omissionController@social_media', compact('id'));
		
	   }else if(strpos($product->get_title(), 'Growthtroop') !== FALSE){
		   
		   	
		   if($updated_view == "index"){
			   Log::info("Entro en index: {$updated_view}");
		   	return redirect()->action('Wp_omissionController@suite', compact('id'));
		   }else{
			   
			   if($updated_view == "web"){		
				     
	            	 return redirect()->action('Wp_omissionController@web', compact('id'));
					 
			   	}else if($updated_view == "web_development"){
		
		         return redirect()->action('Wp_omissionController@web_development', compact('id'));
	
	          }
			
		   }		   
	   }else if((strpos($product->get_title(), 'Web') !== FALSE) || (strpos($product->get_title(), 'Site') !== FALSE) || (strpos($product->get_title(), 'Store') !== FALSE)){
		   if($wp_omission->web_state == "In progress"){
		   	  return redirect()->action('Wp_omissionController@web_development', compact('id'));
		   }else{
		   	 return redirect()->action('Wp_omissionController@web', compact('id'));
		   }
		   
	   }else if(strpos($product->get_title(), 'Infographic') !== FALSE){
		   
	   	   return redirect()->action('Wp_omissionController@infographic', compact('id'));
		   
	   }else if(strpos($product->get_title(), 'Logo') !== FALSE){
		   
	   	  return redirect()->action('Wp_omissionController@logo', compact('id'));
	   }
		
	}
	
	public function save_omission(){
		
		$wp_omission = Wp_omission::findOrFail(input::get('omission_id'));
		
		Log::info("hola save_omission_ajax");
		
		if(input::get('db_field_name') == "blog_title"){
		  $wp_omission->blog_title = input::get('data');
		}else if(input::get('db_field_name') == "blog_examples"){
		  $wp_omission->blog_examples = input::get('data');
		}else if(input::get('db_field_name') == "blog_guidelines"){
		  $wp_omission->blog_guidelines = input::get('data');
		}else if(input::get('db_field_name') == "blog_keywords"){
		  $wp_omission->blog_keywords = input::get('data');
		}else if(input::get('db_field_name') == "blog_goal_improve_seo"){
		  $wp_omission->blog_goal_improve_seo = input::get('data');
		}else if(input::get('db_field_name') == "blog_goal_become_a_thought_leader"){
		  $wp_omission->blog_goal_become_a_thought_leader = input::get('data');
		}else if(input::get('db_field_name') == "blog_goal_recycle_existing_content"){
		  $wp_omission->blog_goal_recycle_existing_content = input::get('data');
		}else if(input::get('db_field_name') == "blog_goal_provide_instructions"){
		  $wp_omission->blog_goal_provide_instructions = input::get('data');
		}else if(input::get('db_field_name') == "blog_tone_serious_experienced_formal"){
		  $wp_omission->blog_tone_serious_experienced_formal = input::get('data');
		}else if(input::get('db_field_name') == "blog_tone_funny_witty_satirical"){
		  $wp_omission->blog_tone_funny_witty_satirical = input::get('data');
		}else if(input::get('db_field_name') == "blog_tone_empathetic_instructional_detailed"){
		  $wp_omission->blog_tone_empathetic_instructional_detailed = input::get('data');
		}else if(input::get('db_field_name') == "blog_tone_casual_conversational_friendly"){
		  $wp_omission->blog_tone_casual_conversational_friendly = input::get('data');
		}else if(input::get('db_field_name') == "web_design_domain"){
		  $wp_omission->web_design_domain = input::get('data');
		}else if(input::get('db_field_name') == "web_design_contact_form_email"){
		  $wp_omission->web_design_contact_form_email = input::get('data');
		}else if(input::get('db_field_name') == "web_design_admin_username"){
		  $wp_omission->web_design_admin_username = input::get('data');
		}else if(input::get('db_field_name') == "web_design_admin_desired_password"){
		  $wp_omission->web_design_admin_desired_password = input::get('data');
		}else if(input::get('db_field_name') == "company_address"){
		  $wp_omission->company_address = input::get('data');
		}else if(input::get('db_field_name') == "company_phones"){
		  $wp_omission->company_phones = input::get('data');
		}else if(input::get('db_field_name') == "company_portfolio"){
		  $wp_omission->company_portfolio = input::get('data');
		}else if(input::get('db_field_name') == "web_design_examples"){
		  $wp_omission->web_design_examples = input::get('data');
		}else if(input::get('db_field_name') == "company_domain_keys"){
		  $wp_omission->company_domain_keys = input::get('data');
		}else if(input::get('db_field_name') == "web_design_social_urls"){
		  $wp_omission->web_design_social_urls = input::get('data');
		}else if(input::get('db_field_name') == "company_name"){
		  $wp_omission->company_name = input::get('data');
		}else if(input::get('db_field_name') == "project_manager"){
		  $wp_omission->project_manager = input::get('data');
		}else if(input::get('db_field_name') == "project_manager_phone"){
		  $wp_omission->project_manager_phone = input::get('data');
		}else if(input::get('db_field_name') == "project_manager_email"){
		  $wp_omission->project_manager_email = input::get('data');
		}else if(input::get('db_field_name') == "company_address"){
		  $wp_omission->company_address = input::get('data');
		}else if(input::get('db_field_name') == "company_phones"){
		  $wp_omission->company_phones = input::get('data');
		}else if(input::get('db_field_name') == "company_url"){
		  $wp_omission->company_url = input::get('data');
		}else if(input::get('db_field_name') == "infographic_title"){
		  $wp_omission->infographic_title = input::get('data');
		}else if(input::get('db_field_name') == "infographic_content"){
		  $wp_omission->infographic_content = input::get('data');
		}else if(input::get('db_field_name') == "infographic_keywords"){
		  $wp_omission->infographic_keywords = input::get('data');
		}else if(input::get('db_field_name') == "infographic_examples"){
		  $wp_omission->infographic_examples = input::get('data');
		}else if(input::get('db_field_name') == "infographic_guidelines"){
		  $wp_omission->infographic_guidelines = input::get('data');
		}else if(input::get('db_field_name') == "logo_keywords"){
		  $wp_omission->logo_keywords = input::get('data');
		}else if(input::get('db_field_name') == "logo_examples"){
		  $wp_omission->logo_examples = input::get('data');
		}else if(input::get('db_field_name') == "logo_guidelines"){
		  $wp_omission->logo_guidelines = input::get('data');
		}else if(input::get('db_field_name') == "web_design_examples"){
		  $wp_omission->web_design_examples = input::get('data');
		}else if(input::get('db_field_name') == "company_domain_keys"){
		  $wp_omission->company_domain_keys = input::get('data');
		}else if(input::get('db_field_name') == "web_design_social_urls"){
		  $wp_omission->web_design_social_urls = input::get('data');
		}else if(input::get('db_field_name') == "web_design_keywords"){
		  $wp_omission->web_design_keywords = input::get('data');
		}else if(input::get('db_field_name') == "web_design_guidelines"){
		  $wp_omission->web_design_guidelines = input::get('data');
		}else if(input::get('db_field_name') == "web_design_admin_site_name"){
		  $wp_omission->web_design_admin_site_name = input::get('data');
		}else if(input::get('db_field_name') == "web_design_section1_title"){
		  $wp_omission->web_design_section1_title = input::get('data');
		}else if(input::get('db_field_name') == "web_design_section2_title"){
		  $wp_omission->web_design_section2_title = input::get('data');
		}else if(input::get('db_field_name') == "web_design_section3_title"){
		  $wp_omission->web_design_section3_title = input::get('data');
		}else if(input::get('db_field_name') == "web_design_section4_title"){
		  $wp_omission->web_design_section4_title = input::get('data');
		}else if(input::get('db_field_name') == "web_design_section5_title"){
		  $wp_omission->web_design_section5_title = input::get('data');
		}else if(input::get('db_field_name') == "web_design_section6_title"){
		  $wp_omission->web_design_section6_title = input::get('data');
		}else if(input::get('db_field_name') == "web_design_section7_title"){
		  $wp_omission->web_design_section7_title = input::get('data');
		}else if(input::get('db_field_name') == "web_design_section1_content"){
		  $wp_omission->web_design_section1_content = input::get('data');
		}else if(input::get('db_field_name') == "web_design_section2_content"){
		  $wp_omission->web_design_section2_content = input::get('data');
		}else if(input::get('db_field_name') == "web_design_section3_content"){
		  $wp_omission->web_design_section3_content = input::get('data');
		}else if(input::get('db_field_name') == "web_design_section4_content"){
		  $wp_omission->web_design_section4_content = input::get('data');
		}else if(input::get('db_field_name') == "web_design_section5_content"){
		  $wp_omission->web_design_section5_content = input::get('data');
		}else if(input::get('db_field_name') == "web_design_section6_content"){
		  $wp_omission->web_design_section6_content = input::get('data');
		}else if(input::get('db_field_name') == "web_design_section7_content"){
		  $wp_omission->web_design_section7_content = input::get('data');
		}else if(input::get('db_field_name') == "web_design_domain_user_and_password"){
		  $wp_omission->web_design_domain_user_and_password = input::get('data');
		}else if(input::get('db_field_name') == "web_design_domain_user_and_password"){
		  $wp_omission->web_design_domain_user_and_password = input::get('data');
		}else if(input::get('db_field_name') == "web_state"){
		  $wp_omission->web_state = input::get('data');
		}
		

		$wp_omission->save();
		
		return response()->json($wp_omission);
		
	}
	
	public function article($id){
	
 	   $viewsw = "my_missions";
 	   $wp_omission = Wp_omission::findOrFail($id);
 	   $product = get_product($wp_omission->product_id);
	   
	   $files = DB::select("select * from wp_omissions_files where omission_id = {$wp_omission->id} and section = 'article'");
	   
	   return view('wp_omissions.article', compact('wp_omission','viewsw','product','files'));
	}
	
	
	public function developer($id){
	
 	   $viewsw = "my_missions";
 	   $wp_omission = Wp_omission::findOrFail($id);
 	   $product = get_product($wp_omission->product_id);
	   
	   $files = DB::select("select * from wp_omissions_files where omission_id = {$wp_omission->id} and section = 'developer'");
	   
	   return view('wp_omissions.developer', compact('wp_omission','viewsw','product','files'));
	}
	
	public function logo($id){include 'Wp_omissionController.php';
	
		
  	   $viewsw = "my_missions";
  	   $wp_omission = Wp_omission::findOrFail($id);
  	   $product = get_product($wp_omission->product_id);
	   
	   $files = DB::select("select * from wp_omissions_files where omission_id = {$wp_omission->id} and section = 'logo_product'");
	   
 	   return view('wp_omissions.logo', compact('wp_omission','viewsw','product','files'));
	   
	}
	
	public function web($id){
  	   $viewsw = "my_missions";
  	   $wp_omission = Wp_omission::findOrFail($id);
  	   $product = get_product($wp_omission->product_id);
	   
	   $styles_ids_array = Wp_omissions_styles_mission::where('omission_id' ,'=' ,$wp_omission->id)->lists('omission_style_id')->toArray();
	   $wp_omissions_styles = Wp_omissions_style::orderBy('id', 'desc')->paginate(30);
	   
	   $mockup_files = DB::select("select * from wp_omissions_files where omission_id = {$wp_omission->id} and section = 'mockup'");
	   $logo_files = DB::select("select * from wp_omissions_files where omission_id = {$wp_omission->id} and section = 'logo'");
	   
	   return view('wp_omissions.web', compact('wp_omission','viewsw','product','mockup_files','logo_files','wp_omissions_styles','styles_ids_array'));
	}
	
	public function web_development($id){
  	   $viewsw = "my_missions";
  	   $wp_omission = Wp_omission::findOrFail($id);
  	   $product = get_product($wp_omission->product_id);
	   
	   //$files = Wp_omissions_file::where('omission_id', '=', $wp_omission->id )->get();
	   $styles_ids_array = Wp_omissions_styles_mission::where('omission_id' ,'=' ,$wp_omission->id)->lists('omission_style_id')->toArray();
	   $wp_omissions_styles = Wp_omissions_style::orderBy('id', 'desc')->paginate(30);
	   
	   $files = DB::select("select * from wp_omissions_files where omission_id = {$wp_omission->id} and section != 'logo' and section != 'mockup'");
	   
	   return view('wp_omissions.web_development', compact('wp_omission','viewsw','product','files','wp_omissions_styles','styles_ids_array'));
	}
	
	public function social_media($id){
		
  	    $viewsw = "my_missions";
  	    $wp_omission = Wp_omission::findOrFail($id);
  	    $product = get_product($wp_omission->product_id);
		
		$files = DB::select("select * from wp_omissions_files where omission_id = {$wp_omission->id} and section = 'social'");
		
		$initial_date = strtotime("last Monday");
	    $last_date = strtotime("next Sunday");	
		$initial_date = date ( 'Y/m/d' , $initial_date );
		$last_date = date ( 'Y/m/d' , $last_date );
		
		$wp_omissions_facebook = DB::select("SELECT * FROM wp_omissions_calendars where social_network = 'facebook' and start_date >= '{$initial_date}' and start_date <= '{$last_date}'");
		
		$wp_omissions_instagram =  DB::select("SELECT * FROM wp_omissions_calendars where social_network = 'instagram' and start_date >= '{$initial_date}' and start_date <= '{$last_date}'");
			
		$wp_omissions_twitter =  DB::select("SELECT * FROM wp_omissions_calendars where social_network = 'twitter' and start_date >= '{$initial_date}' and start_date <= '{$last_date}'");
		
		return view('wp_omissions.social_media', compact('wp_omission','viewsw','product','wp_omissions_facebook','wp_omissions_twitter','wp_omissions_instagram','files'));
	}
	
	public function suite($id){
		
   	   $viewsw = "my_missions";
   	   $wp_omission = Wp_omission::findOrFail($id);
   	   $product = get_product($wp_omission->product_id);
	   
  	   return view('wp_omissions.suite', compact('wp_omission','viewsw','product'));
	}
		
	
	public function infographic($id){
		
  	   $viewsw = "my_missions";
  	   $wp_omission = Wp_omission::findOrFail($id);
  	   $product = get_product($wp_omission->product_id);
	   
	   $files = DB::select("select * from wp_omissions_files where omission_id = {$wp_omission->id} and section = 'infographic'");
	   
 	   return view('wp_omissions.infographic', compact('wp_omission','viewsw','product','files'));
	
	}
	
	public function save_file(Request $request){
		$wp_omissions_file = new Wp_omissions_file();
		$wp_omissions_file->omission_id = Input::get('omission_id');
		$wp_omissions_file->section = Input::get('section');
		$wp_omissions_file->file_type = Input::get('file_type');
		$wp_omissions_file->title = Input::get('title');
		if ($request->file('logofile1')) {
			$file = $request->file('logofile1');
	        $destinationPath = 'files/'.Input::get('omission_id');
	        $extension = $file->getClientOriginalExtension();
	        $fileName = md5(uniqid(rand(), true)) . '.' . $extension;
	        $upload_success = $file->move($destinationPath, $fileName);
			$wp_omissions_file->file = $fileName;
		}
		
		$wp_omissions_file->save();
		//$files = Wp_omissions_file::where('omission_id', '=', Input::get('omission_id') )->get();
		$omission_id = Input::get('omission_id');
		$section = Input::get('section');
		
		$files = DB::select("select * from wp_omissions_files where omission_id = {$omission_id} and section = '{$section}'");
		
		return response()->json($files);
		
	}
	
	
	
	public function delete_file_ajax(){
		
		Log::info("hola delete_omission_ajax");
		Log::info("omissions_file_id:");
		Log::info(input::get('omissions_file_id'));
		Log::info("omission_id:");
		Log::info(input::get('omission_id'));
		
		$wp_omissions_file = Wp_omissions_file::findOrFail(input::get('omissions_file_id'));
		Log::info("id: {$wp_omissions_file->id}");
		
		$wp_omissions_file->delete();
		
		$omission_id = Input::get('omission_id');
		$section = Input::get('section');
		
		$files = DB::select("select * from wp_omissions_files where omission_id = {$omission_id} and section = '{$section}'");
		
		return response()->json($files);
		
	}
	


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$user = Auth::user();
		
		if($user){
			App::setLocale($user->ogrowthlang);	
			$lang = $user->ogrowthlang;
		}
		
		return view('wp_omissions.create',compact('user','lang'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$wp_omission = new Wp_omission();

		$wp_omission->otitle = $request->input("otitle");
		Log::info('wp_missions STORE');
		Log::info('This is some useful information.');
		
		if($request->file('ofile_name')!= ""){
			
			$file = $request->file('ofile_name');
	        // SET UPLOAD PATH
	        $destinationPath = 'oimages';
	         // GET THE FILE EXTENSION
	        $extension = $file->getClientOriginalExtension();
	         // RENAME THE UPLOAD WITH RANDOM NUMBER
	        $fileName = rand(11111, 99999) . '.' . $extension;
	         // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY
	        $upload_success = $file->move($destinationPath, $fileName);
			$wp_omission->ofile_name = $fileName;
		}

		$wp_omission->save();

		return redirect()->route('wp_omissions.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$wp_omission = Wp_omission::findOrFail($id);

		return view('wp_omissions.show', compact('wp_omission'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit_past($id)
	{
		$user = Auth::user();
		
		if($user){
			App::setLocale($user->ogrowthlang);	
			$lang = $user->ogrowthlang;
		}
		
		$viewsw = "my_missions";
		$wp_omission = Wp_omission::findOrFail($id);
		
		$styles_ids_array = Wp_omissions_styles_mission::where('omission_id' ,'=' ,$wp_omission->id)->lists('omission_style_id')->toArray();
		Log::info('estlilos seleccionados: ');
		//Log::info($styles_ids_array);
		$product = get_product($wp_omission->product_id);
		
		$files = Wp_omissions_file::where('omission_id', '=', $wp_omission->id )->get();
		$wp_omissions_styles = Wp_omissions_style::orderBy('id', 'desc')->paginate(30);
		
		#last week calendars
		date_default_timezone_set('America/Bogota');
	
		$initial_date = strtotime("last Monday");
	    $last_date = strtotime("next Sunday");	
		$initial_date = date ( 'Y/m/d' , $initial_date );
		$last_date = date ( 'Y/m/d' , $last_date );
		
		Log::info('querys');
		Log::info("SELECT * FROM wp_omissions_calendars where social_network = 'facebook' and start_date >= {$initial_date} and start_date <= {$last_date}");
		Log::info("SELECT * FROM wp_omissions_calendars where social_network = 'instagram' and start_date >= {$initial_date} and start_date <= {$last_date}");
		Log::info("SELECT * FROM wp_omissions_calendars where social_network = 'twitter' and start_date >= {$initial_date} and start_date <= {$last_date}");
		
		$wp_omissions_facebook = DB::select("SELECT * FROM wp_omissions_calendars where social_network = 'facebook' and start_date >= '{$initial_date}' and start_date <= '{$last_date}'");
		
		$wp_omissions_instagram =  DB::select("SELECT * FROM wp_omissions_calendars where social_network = 'instagram' and start_date >= '{$initial_date}' and start_date <= '{$last_date}'");
			
		$wp_omissions_twitter =  DB::select("SELECT * FROM wp_omissions_calendars where social_network = 'twitter' and start_date >= '{$initial_date}' and start_date <= '{$last_date}'");
		
		return view('wp_omissions.edit', compact('wp_omission','viewsw','product','files','wp_omissions_styles','styles_ids_array','wp_omissions_facebook','wp_omissions_twitter','wp_omissions_instagram'));
	}
	
	
	
	
	

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$wp_omission = Wp_omission::findOrFail($id);
		$post_data = Input::all();
	   	//$wp_omission->fill($post_data);
		Log::info('array: ');
		//Log::info($post_data);
		
		$product = get_product($wp_omission->product_id);
		
		#General
		$wp_omission->state = $request->input("state");
		$wp_omission->company_name = $request->input("company_name");
		$wp_omission->project_manager = $request->input("project_manager");
		$wp_omission->project_manager_phone = $request->input("project_manager_phone");
		$wp_omission->project_manager_email = $request->input("project_manager_email");
		$wp_omission->company_url = $request->input("company_url");
		$wp_omission->company_contact_email = $request->input("company_contact_email");
		$wp_omission->company_address = $request->input("company_address");
		$wp_omission->company_phones = $request->input("company_phones");
		
		$wp_omission->keywords = $request->input("keywords");
		$wp_omission->examples = $request->input("examples");
		$wp_omission->guidelines = $request->input("guidelines");
		
		#Blog article
		if($product->get_title() == "Article Writing"){
			
			$wp_omission->blog_title = $request->input("blog_title");
			$wp_omission->blog_goal_improve_seo = $request->input("blog_goal_improve_seo");
			$wp_omission->blog_goal_become_a_thought_leader = $request->input("blog_goal_become_a_thought_leader");
			$wp_omission->blog_goal_recycle_existing_content = $request->input("blog_goal_recycle_existing_content");
			$wp_omission->blog_goal_provide_instructions = $request->input("blog_goal_provide_instructions");
			$wp_omission->blog_tone_serious_experienced_formal = $request->input("blog_tone_serious_experienced_formal");
			$wp_omission->blog_tone_funny_witty_satirical = $request->input("blog_tone_funny_witty_satirical");
			$wp_omission->blog_tone_empathetic_instructional_detailed = $request->input("blog_tone_empathetic_instructional_detailed");
			$wp_omission->blog_tone_casual_conversational_friendly = $request->input("blog_tone_casual_conversational_friendly");
			
		}else if($product->get_title() == "Blog or Magazine Site"){
			
			#Web site
			$wp_omission->web_design_admin_username = $request->input("web_design_admin_username");
			$wp_omission->web_design_admin_desired_password = $request->input("web_design_admin_desired_password");
			$wp_omission->web_design_admin_site_name = $request->input("web_design_admin_site_name");
			$wp_omission->web_design_domain = $request->input("web_design_domain");
			$wp_omission->web_design_section1_title = $request->input("web_design_section1_title");
			$wp_omission->web_design_section2_title = $request->input("web_design_section2_title");
			$wp_omission->web_design_section3_title = $request->input("web_design_section3_title");
			$wp_omission->web_design_section4_title = $request->input("web_design_section4_title");
			$wp_omission->web_design_section5_title = $request->input("web_design_section5_title");
			$wp_omission->web_design_section6_title = $request->input("web_design_section6_title");
			$wp_omission->web_design_section7_title = $request->input("web_design_section7_title");
			$wp_omission->web_design_section1_content = $request->input("web_design_section1_content");
			$wp_omission->web_design_section2_content = $request->input("web_design_section2_content");
			$wp_omission->web_design_section3_content = $request->input("web_design_section3_content");
			$wp_omission->web_design_section4_content = $request->input("web_design_section4_content");
			$wp_omission->web_design_section5_content = $request->input("web_design_section5_content");
			$wp_omission->web_design_section6_content = $request->input("web_design_section6_content");
			$wp_omission->web_design_section7_content = $request->input("web_design_section7_content");
			$wp_omission->web_design_contact_form_email = $request->input("web_design_contact_form_email");
			$wp_omission->web_design_social_urls = $request->input("web_design_social_urls");
			$wp_omission->web_design_domain_user_and_password = $request->input("web_design_domain_user_and_password");
			$wp_omission->company_name = $request->input("company_name");
			$wp_omission->project_manager = $request->input("project_manager");
			$wp_omission->project_manager_phone = $request->input("project_manager_phone");
			$wp_omission->project_manager_email = $request->input("project_manager_email");
			$wp_omission->company_address = $request->input("company_address");
			$wp_omission->company_phones = $request->input("company_phones");
			$wp_omission->company_url = $request->input("company_url");
			$wp_omission->company_domain_keys = $request->input("company_domain_keys");
			$wp_omission->company_contact_email = $request->input("company_contact_email");
			$wp_omission->company_portfolio = $request->input("company_portfolio");
			
			for ($x = 1; $x <= 35; $x++) {
		
			   if($request->file("file{$x}")!= ""){
				Log::info("Entro ciclo {$x}");
			
				$wp_omissions_file = new Wp_omissions_file();
				$wp_omissions_file->omission_id = $request->input("omission_id");
				$wp_omissions_file->title = $request->input("title{$x}");
				$wp_omissions_file->section = $request->input("file_section");	
				$file = $request->file("file{$x}");
		        // SET UPLOAD PATH
		        $destinationPath = "files/{$wp_omission->id}";
		         // GET THE FILE EXTENSION
		        $extension = $file->getClientOriginalExtension();
		         // RENAME THE UPLOAD WITH RANDOM NUMBER
		        $fileName = rand(11111, 99999) . '.' . $extension;
		         // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY
		        $upload_success = $file->move($destinationPath, $fileName);
				$wp_omissions_file->file = $fileName;
				
					
				$wp_omissions_file->save();
			   }
		
			} 
		
			for ($x = 1; $x <= 5; $x++) {
		
			   if($request->file("logofile{$x}")!= ""){
				Log::info("Entro ciclo {$x}");
			
				$wp_omissions_file = new Wp_omissions_file();
				$wp_omissions_file->omission_id = $request->input("omission_id");
				$wp_omissions_file->title = $request->input("logotitle{$x}");
				$wp_omissions_file->section = $request->input("file_section");	
				$file = $request->file("logofile{$x}");
		        // SET UPLOAD PATH
		        $destinationPath = "files/{$wp_omission->id}";
		         // GET THE FILE EXTENSION
		        $extension = $file->getClientOriginalExtension();
		         // RENAME THE UPLOAD WITH RANDOM NUMBER
		        $fileName = rand(11111, 99999) . '.' . $extension;
		         // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY
		        $upload_success = $file->move($destinationPath, $fileName);
				$wp_omissions_file->file = $fileName;
			
				$wp_omissions_file->save();
			   }
		
			} 
		
		
			for ($x = 1; $x <= 5; $x++) {
		
			   if($request->file("mockupfile{$x}")!= ""){
				Log::info("Entro ciclo {$x}");
			
				$wp_omissions_file = new Wp_omissions_file();
				$wp_omissions_file->omission_id = $request->input("omission_id");
				$wp_omissions_file->title = $request->input("mockuptitle{$x}");
				$wp_omissions_file->section = $request->input("file_section");	
				$file = $request->file("mockupfile{$x}");
		        // SET UPLOAD PATH
		        $destinationPath = "files/{$wp_omission->id}";
		         // GET THE FILE EXTENSION
		        $extension = $file->getClientOriginalExtension();
		         // RENAME THE UPLOAD WITH RANDOM NUMBER
		        $fileName = rand(11111, 99999) . '.' . $extension;
		         // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY
		        $upload_success = $file->move($destinationPath, $fileName);
				$wp_omissions_file->file = $fileName;
			
				$wp_omissions_file->save();
			   }
		
			} 
		
			$affected = DB::table('wp_omissions_styles_missions')->where('omission_id', '=', $wp_omission->id)->delete();
		
			if(isset($post_data["webstlyes"])){
				$array = $post_data["webstlyes"];
				foreach ($array as $object) {
				    Log::info('arrayitem: ' . $object);
					$styles_missions = new Wp_omissions_styles_mission();
					$styles_missions->omission_id = $wp_omission->id;
				    $styles_missions->omission_style_id = $object;
					$styles_missions->save();
				}
			}
			
		}else if($product->get_title() == "Social Media Level One"){
			
			for ($x = 1; $x <= 5; $x++) {
		
			   if($request->file("logofile{$x}")!= ""){
				Log::info("Entro ciclo {$x}");
			
				$wp_omissions_file = new Wp_omissions_file();
				$wp_omissions_file->omission_id = $request->input("omission_id");
				$wp_omissions_file->title = $request->input("logotitle{$x}");
				$wp_omissions_file->section = $request->input("file_section");	
				$file = $request->file("logofile{$x}");
		        // SET UPLOAD PATH
		        $destinationPath = "files/{$wp_omission->id}";
		         // GET THE FILE EXTENSION
		        $extension = $file->getClientOriginalExtension();
		         // RENAME THE UPLOAD WITH RANDOM NUMBER
		        $fileName = rand(11111, 99999) . '.' . $extension;
		         // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY
		        $upload_success = $file->move($destinationPath, $fileName);
				$wp_omissions_file->file = $fileName;
			
				$wp_omissions_file->save();
			   }
		
			} 
			
			$wp_omission->company_description = $request->input("company_description");
			$wp_omission->company_mission = $request->input("company_mission");
			$wp_omission->company_vision = $request->input("company_vision");
			$wp_omission->company_description = $request->input("company_description");
			$wp_omission->company_target = $request->input("company_target");
			$wp_omission->company_promos = $request->input("company_promos");
			$wp_omission->company_portfolio = $request->input("company_portfolio");
			$wp_omission->company_portfolio = $request->input("company_portfolio");
			$wp_omission->company_corporative_values = $request->input("company_corporative_values");
			
			$wp_omission->calendar_days = $request->input("calendar_days");
			$wp_omission->calendar_number_of_publications = $request->input("calendar_number_of_publications");
			
			$wp_omission->calendar_time_template_1 = $request->input("calendar_time_template_1");
			$wp_omission->calendar_time_template_2 = $request->input("calendar_time_template_2");
			$wp_omission->calendar_time_template_3 = $request->input("calendar_time_template_3");
			$wp_omission->calendar_time_template_4 = $request->input("calendar_time_template_4");
			$wp_omission->calendar_time_template_5 = $request->input("calendar_time_template_5");
			$wp_omission->calendar_time_template_6 = $request->input("calendar_time_template_6");
			
			if($request->input("calendar_facebook") == true){
				Log::info("Entro en facebook");
				$calendar_days = (int)$request->input("calendar_days");
				for ($x = 1; $x <= $calendar_days; $x++) {
					$number_of_publications = (int)$request->input("calendar_number_of_publications");
					
					for ($y = 1; $y <= $number_of_publications; $y++) {
					
						$facebook = new Wp_omissions_calendar();
						$facebook->social_network = "facebook";
						$facebook->omission_id = $wp_omission->id;
						$date = date_create($request->input("calendar_date_since"));
						date_add($date,date_interval_create_from_date_string("{$x} days"));
						$date->modify($request->input("calendar_time_template_{$y}")); 
						$facebook->start_date = $date;
				    	$facebook->save();
					}
					
				}
			}
			
			if($request->input("calendar_instagram") == true){
				Log::info("Entro Instagram");
				$calendar_number_of_days = (int)$request->input("calendar_days");
				for ($x = 1; $x <= $calendar_number_of_days; $x++) {
					
					$number_of_publications = (int)$request->input("calendar_number_of_publications");
					
					for ($y = 1; $y <= $number_of_publications; $y++) {
					
						$instagram = new Wp_omissions_calendar();
						$instagram->social_network = "instagram" ;
						$instagram->omission_id = $wp_omission->id;
						$date = date_create($request->input("calendar_date_since"));
						date_add($date,date_interval_create_from_date_string("{$x} days"));
						$date->modify($request->input("calendar_time_template_{$y}")); 
						$instagram->start_date = $date;
				    	$instagram->save();
					}
					
				}
			}
			
			
			if($request->input("calendar_twitter") == true){
				Log::info("Entro Twitter");
				$calendar_number_of_days = (int)$request->input("calendar_days");
				for ($x = 1; $x <= $calendar_number_of_days; $x++) {
					Log::info("Entro x loop");
					$number_of_publications = (int)$request->input("calendar_number_of_publications");
					
					for ($y = 1; $y <= $number_of_publications; $y++) {
						Log::info("Entro y loop");
						$twitter = new Wp_omissions_calendar();
						$twitter->social_network = "twitter";
						$twitter->omission_id = $wp_omission->id;
						$date = date_create($request->input("calendar_date_since"));
						date_add($date,date_interval_create_from_date_string("{$x} days"));
						$date->modify($request->input("calendar_time_template_{$y}")); 
						$twitter->start_date = $date;
				    	$twitter->save();
					}
					
				}
			}
			
		}
		
		
		$wp_omission->save();
		

		return redirect()->route('wp_omissions.edit', $wp_omission->id)->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$wp_omission = Wp_omission::findOrFail($id);
		$wp_omission->delete();

		return redirect()->route('wp_omissions.index')->with('message', 'Item deleted successfully.');
	}

}
