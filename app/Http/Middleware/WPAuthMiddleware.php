<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use App\User;
use Illuminate\Contracts\Auth\Guard;

class WPAuthMiddleware
{
    protected $wp_user;

    public function __construct(Guard $auth)
    {
        global $current_user;
        $this->wp_user = getUserWP();
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($this->wp_user->ID > 0) {
            $user = User::find($this->wp_user->ID);
            Auth::login($user);
        } else {

			if(isset($user)){
				Auth::logout($user);
			}

            //Auth::logout($user);

            return redirect(env('WP_URL').'/my-account');
        }
        return $next($request);
    }
}
