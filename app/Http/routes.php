<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('set_lang','Wp_omissionController@set_lang');
Route::get('set_omission','Wp_omissionController@set_omission');
Route::get('/delete_file/{id}', 'Wp_omissionController@delete_file');
Route::get('/set_calendar', 'Wp_omissionController@set_calendar');
Route::get('/get_calendars', 'Wp_omissionController@get_calendars');

Route::get('/logo/{id}', 'Wp_omissionController@logo');
Route::get('/article/{id}', 'Wp_omissionController@article');
Route::get('/web/{id}', 'Wp_omissionController@web');
Route::get('/web_development/{id}', 'Wp_omissionController@web_development');
Route::get('/social_media/{id}', 'Wp_omissionController@social_media');
Route::get('/suite/{id}', 'Wp_omissionController@suite');
Route::get('/infographic/{id}', 'Wp_omissionController@infographic');
Route::get('/developer/{id}', 'Wp_omissionController@developer');



Route::post('/create_card', 'Wp_omissionController@create_card');
Route::post('/update_card', 'Wp_omissionController@update_card');
Route::post('/delete_card', 'Wp_omissionController@delete_card');

Route::post('/create_section', 'Wp_omissionController@create_section');
Route::post('/update_section', 'Wp_omissionController@update_section');
Route::post('/delete_section', 'Wp_omissionController@delete_section');


Route::post('/set_calendar_image', 'Wp_omissionController@set_calendar_image');
Route::post('/save_file', 'Wp_omissionController@save_file');
Route::post('/save_file_html', 'Wp_omissionController@save_file_html');
Route::post('/delete_file_ajax', 'Wp_omissionController@delete_file_ajax');
Route::post('/save_omission', 'Wp_omissionController@save_omission');


Route::resource("wp_omissions","Wp_omissionController");
Route::resource("wp_omissions_files","Wp_omissions_fileController");
Route::resource("wp_omissions_styles","Wp_omissions_styleController");
Route::resource("wp_omissions_calendars","Wp_omissions_calendarController");

Route::get('/', array('as' => 'wp_omissions.index', 'uses' => 'Wp_omissionController@index'));
//Route::get('/home', array('as' => 'ogrowthmissions.index', 'uses' => 'OgrowthmissionController@index'));



