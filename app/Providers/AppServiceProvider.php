<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use View;
use App;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
	   	View::composer('*', function($view){
		
		  
		  $current_user = Auth::user();		
	   	  if(isset($current_user)){
			  if(isset($current_user->ogrowthlang)){
	   	  	 	 App::setLocale($current_user->ogrowthlang);
			 }
		    $view->with(compact('current_user'));
	   	  }
		 
	  
	    });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
