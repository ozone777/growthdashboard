<?php
// My common functions
function somethingOrOther()
{
    return (mt_rand(1,2) == 1) ? 'something' : 'other';
}

function getIcon($category)
{
	
	if($category == "1"){
		return "<img src='/growthimages/product_writing.jpg'>";
	}elseif ($category == "2"){
		return "<img src='/growthimages/product_infographics.jpg'>";
    }elseif ($category == "3"){
		return "<img src='/growthimages/product_socialmediamgmt.jpg'>";
    }elseif ($category == "4"){
		return "<img src='/growthimages/product_socialmediamgmt.jpg'>";
    }elseif ($category == "5"){
		return "<img src='/growthimages/product_socialmediamgmt.jpg'>";
    }elseif ($category == "6"){
		return "<img src='/growthimages/product_onlineshop.jpg'>";
    }elseif ($category == "7"){
		return "<img src='/growthimages/product_magazinesite.jpg'>";
    }elseif ($category == "8"){
		return "<img src='/growthimages/product_portfoliosite.jpg'>";
    }elseif ($category == "9"){
		return "<img src='/growthimages/product_landingpage.jpg'>";
    } 
}



function get_mission_category_name($category){
	
	if($category == "1"){
		return trans('categories.blog_post');
	}elseif ($category == "2"){
		return trans('categories.infographic');
    }elseif ($category == "3"){
		return trans('categories.social_media_level_one');
    }elseif ($category == "4"){
		return trans('categories.social_media_level_two');
    }elseif ($category == "5"){
		return trans('categories.social_media_level_three');
    }elseif ($category == "6"){
		return trans('categories.online_store');
    }elseif ($category == "7"){
		return trans('categories.blog_magazine');
    }elseif ($category == "8"){
		return trans('categories.portfolio');
    }elseif ($category == "9"){
		return trans('categories.landing_page');
    } 
	
}

/*


		DB::table('ogrowthcategories')->insert(['id' => '1','name' => 'Blog Post','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('ogrowthcategories')->insert(['id' => '2','name' => 'Infographic','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('ogrowthcategories')->insert(['id' => '3','name' => 'Social Media Level 1','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('ogrowthcategories')->insert(['id' => '4','name' => 'Social Media Level 2','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('ogrowthcategories')->insert(['id' => '5','name' => 'Social Media Level 3','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('ogrowthcategories')->insert(['id' => '6','name' => 'Online Store','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('ogrowthcategories')->insert(['id' => '7','name' => 'Blog / Magazine','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('ogrowthcategories')->insert(['id' => '8','name' => 'Portfolio','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('ogrowthcategories')->insert(['id' => '9','name' => 'Landing Page','created_at' => new DateTime, 'updated_at' => new DateTime]);

	'blog_post' => 'Blog Post',
	'infographic' => 'Infographic',
	'social_media_level_one' => 'Social Media Level 1',
	'social_media_level_two' => 'Social Media Level 2',
	'social_media_level_three' => 'Social Media Level 3',
	'online_store' => 'Online Store',
	'blog_magazine' => 'Blog / Magazine',
	'portfolio' => 'Portfolio',
    'landing_page' => 'Landing Page',
*/



?>