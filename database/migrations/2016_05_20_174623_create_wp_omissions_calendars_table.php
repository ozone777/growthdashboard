<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWpOmissionsCalendarsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('wp_omissions_calendars', function(Blueprint $table) {
            $table->increments('id');
            $table->datetime('start_date');
            $table->string('social_network');
            $table->text('content');
            $table->string('link');
            $table->text('file');
            $table->integer('omission_id');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('wp_omissions_calendars');
	}

}
