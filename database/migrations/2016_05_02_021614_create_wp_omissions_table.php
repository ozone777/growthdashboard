<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWpOmissionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('wp_omissions', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('ogrowthcategory_id')->nullable();
            $table->integer('ogrowthlanguage_id')->nullable();
			
            $table->boolean('completed')->nullable();
            $table->boolean('accepted')->nullable();
            $table->boolean('in_progress')->nullable();
			$table->boolean('suspended')->nullable();
            $table->boolean('aborted')->nullable();
            $table->boolean('ready_for_revision')->nullable();
			
			$table->text('keywords')->nullable();
			$table->text('examples')->nullable();
			$table->text('guidelines')->nullable();
			
			//Blog post goals fields
			$table->string('blog_title')->nullable();
		    $table->boolean('blog_goal_improve_seo')->nullable();
			$table->boolean('blog_goal_become_a_thought_leader')->nullable();
			$table->boolean('blog_goal_recycle_existing_content')->nullable();
			$table->boolean('blog_goal_provide_instructions')->nullable();	
			$table->boolean('blog_tone_serious_experienced_formal')->nullable();
			$table->boolean('blog_tone_funny_witty_satirical')->nullable();
			$table->boolean('blog_tone_empathetic_instructional_detailed')->nullable();
			$table->boolean('blog_tone_casual_conversational_friendly')->nullable();
			
			//Web design general fields
            $table->string('color1')->nullable();
            $table->string('color2')->nullable();
            $table->string('color3')->nullable();
            $table->string('color4')->nullable();
            $table->string('color5')->nullable();
			$table->text('web_design_admin_email')->nullable();
			$table->text('web_design_admin_username')->nullable();
			$table->text('web_design_admin_desired_password')->nullable();
			$table->text('web_design_admin_site_name')->nullable();
			$table->string('web_design_domain')->nullable();
			$table->string('web_design_logo')->nullable();
			$table->string('web_design_section1_title')->nullable();
			$table->string('web_design_section2_title')->nullable();
			$table->string('web_design_section3_title')->nullable();
			$table->string('web_design_section4_title')->nullable();
			$table->string('web_design_section5_title')->nullable();
			$table->string('web_design_section6_title')->nullable();
			$table->string('web_design_section7_title')->nullable();
			$table->text('web_design_section1_content')->nullable();	
			$table->text('web_design_section2_content')->nullable();	
			$table->text('web_design_section3_content')->nullable();	
			$table->text('web_design_section4_content')->nullable();	
			$table->text('web_design_section5_content')->nullable();	
			$table->text('web_design_section6_content')->nullable();	
			$table->text('web_design_section7_content')->nullable();	
			$table->string('web_design_contact_form_email')->nullable();
			$table->text('web_design_social_urls')->nullable();	
			$table->text('web_design_domain_user_and_password')->nullable();	
			
			//Store fields online store
			$table->text('store_types_of_products')->nullable();
			$table->boolean('store_product_fields_size')->nullable();
			$table->boolean('store_product_fields_color')->nullable();
			$table->boolean('store_product_fields_price')->nullable();
			$table->boolean('store_product_fields_weight')->nullable();
			$table->boolean('store_product_fields_images')->nullable();
			$table->string('store_currency')->nullable();
			$table->text('store_delivery_options')->nullable();
			$table->text('store_delivery_areas')->nullable();
			$table->string('store_location')->nullable();
			$table->boolean('store_product_checkout_yes')->nullable();
			$table->boolean('store_product_checkout_no')->nullable();
			
			//Landing page
			$table->boolean('landing_lead_mailchimp')->nullable();
			$table->boolean('landing_lead_csv')->nullable();
			$table->boolean('landing_lead_email')->nullable();
			$table->string('landing_lead_admin_email')->nullable();
			$table->string('landing_main_cta')->nullable();
			
            //Socal Media
			$table->text('social_media_users_and_passwords')->nullable();
			
			//Infographic
			$table->string('infographic_title')->nullable();
			$table->text('infographic_content')->nullable();
			
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('wp_omissions');
	}

}
