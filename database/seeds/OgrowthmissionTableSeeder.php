<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class OgrowthmissionTableSeeder extends Seeder {

    public function run()
    {
        // TestDummy::times(20)->create('App\Post');
		//Para inegrarlo con wordpress y wocommerce se debera habilitar una columna de nombre por cada idioma activo en el .com
		
		/*
		DB::table('ogrowthcategories')->delete();
		DB::table('ogrowthcategories')->insert(['id' => '1','name' => 'Blog Post','es_name' => 'Diseño de Infografía','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('ogrowthcategories')->insert(['id' => '2','name' => 'Infographic','es_name' => '','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('ogrowthcategories')->insert(['id' => '3','name' => 'Social Media Level 1','es_name' => '','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('ogrowthcategories')->insert(['id' => '4','name' => 'Social Media Level 2','es_name' => '','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('ogrowthcategories')->insert(['id' => '5','name' => 'Social Media Level 3','es_name' => '','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('ogrowthcategories')->insert(['id' => '6','name' => 'Online Store','es_name' => '','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('ogrowthcategories')->insert(['id' => '7','name' => 'Blog / Magazine','es_name' => '','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('ogrowthcategories')->insert(['id' => '8','name' => 'Portfolio','es_name' => '','es_name' => '','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('ogrowthcategories')->insert(['id' => '9','name' => 'Landing Page','es_name' => '','created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		DB::table('ogrowthlanguages')->delete();
		DB::table('ogrowthlanguages')->insert(['id' => '1','name' => 'English','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('ogrowthlanguages')->insert(['id' => '2','name' => 'Español','created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		/*
		DB::table('visuals')->delete();
		DB::table('visuals')->insert(['id' => '1','name' => 'Visual1','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('visuals')->insert(['id' => '2','name' => 'Visual2','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('visuals')->insert(['id' => '3','name' => 'Visual3','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('visuals')->insert(['id' => '4','name' => 'Visual4','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('visuals')->insert(['id' => '5','name' => 'Visual5','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('visuals')->insert(['id' => '6','name' => 'Visual6','created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		
		DB::table('ogrowthstates')->delete();
		DB::table('ogrowthstates')->insert(['id' => '1','name' => 'ready_for_revision','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('ogrowthstates')->insert(['id' => '2','name' => 'in_progress','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('ogrowthstates')->insert(['id' => '3','name' => 'accepted','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('ogrowthstates')->insert(['id' => '4','name' => 'completed','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('ogrowthstates')->insert(['id' => '5','name' => 'suspended','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('ogrowthstates')->insert(['id' => '6','name' => 'aborted','created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		*/
		
	
		
		DB::table('ogrowthmissions')->delete();
		DB::table('ogrowthmissions')->insert(['id' => '1','name' => 'Article Writing','name_es' => 'Escribir Articulo','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('ogrowthmissions')->insert(['id' => '2','name' => 'Blog or Magazine Site','name_es' => 'Sitio Web Para Revistas','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('ogrowthmissions')->insert(['id' => '3','name' => 'Infographic Design','name_es' => 'Diseño de Infografía','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('ogrowthmissions')->insert(['id' => '4','name' => 'Landing Page','name_es' => 'Página de Aterrizaje','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('ogrowthmissions')->insert(['id' => '5','name' => 'Logo Design','name_es' => 'Diseño de Logo','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('ogrowthmissions')->insert(['id' => '6','name' => 'Online Shop Design','name_es' => 'Tienda Online','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('ogrowthmissions')->insert(['id' => '7','name' => 'Portfolio Site','name_es' => 'Sitio Web De Negocios','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('ogrowthmissions')->insert(['id' => '8','name' => 'Social Media Level One','name_es' => 'Redes Sociales Nivel Uno','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('ogrowthmissions')->insert(['id' => '9','name' => 'Social Media Level Three','name_es' => 'Redes Sociales Nivel Tres','created_at' => new DateTime, 'updated_at' => new DateTime]);
		DB::table('ogrowthmissions')->insert(['id' => '10','name' => 'Social Media Level Two','name_es' => 'Redes Sociales Nivel Dos','created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		
    }

}