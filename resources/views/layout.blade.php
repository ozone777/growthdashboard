<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href=/"assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>GrowthTroop</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="/assets/css/jquery.stepy.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="/assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>

    <!--  GrowthTroop CSS    -->
    <link href="/assets/css/growthtroop.css" rel="stylesheet"/>
	
	 <link type="text/css" rel="stylesheet" href="/assets/css/jquery.stepy.css" />

    <!--     Fonts and icons     
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	-->
	<link href="/assets/css/font-awesome.min.css" rel="stylesheet"/>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
	 <!-- 
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	-->
	<script src="/assets/js/jquery.min.js"></script>
		 
	<script src="/assets/js/jquery.tagsinput.js"></script>
	<link rel="stylesheet" type="text/css" href="/assets/css/jquery.tagsinput.css" />
	
	<script src="/assets/js/ckeditor.js"></script>
    <script src="/assets/js/jquery.js"></script>
	
	
	 <script type="text/javascript" src="/assets/js/jquery.stepy.js"></script>
	
	<link rel="stylesheet" type="text/css" href="/assets/css/jquery.datetimepicker.css"/>
	<script src="/assets/js/jquery.datetimepicker.full.min.js"></script>
	<script src="/assets/js/jquery.validate.min.js"></script>
	
    <script type="text/javascript" src="/assets/js/jquery.timepicker.js"></script>
	<script type="text/javascript" src="/assets/js/jquery.observe_field.js"></script>
	<script type="text/javascript" src="/assets/js/underscore.min.js"></script>
	
	<script type="text/javascript" src="/assets/js/application.js"></script>
	
	<script type='text/javascript' src='/assets/js/unitegallery/js/unitegallery.min.js'></script> 
	<link rel='stylesheet' href='/assets/js/unitegallery/css/unite-gallery.css' type='text/css' /> 
	<script type='text/javascript' src='/assets/js/unitegallery/themes/default/ug-theme-default.js'></script> 
	<link rel='stylesheet' href='/assets/js/unitegallery/themes/default/ug-theme-default.css' type='text/css' />
	
    <link rel="stylesheet" type="text/css" href="/assets/css/jquery.timepicker.css" />

    <script type="text/javascript" src="/assets/js/bootstrap-datepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-datepicker.css" />
	<script type="text/javascript" src="/assets/js/jquery.uploadPreview.min.js"></script>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	
</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-color="teal" data-image="/fondo_nuevo.jpg">

    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="http://www.creative-tim.com" class="simple-text">
                <img src="/assets/img/logo.png"/>
                </a>
            </div>

            <ul class="nav">
				
				<?php
				$user = new WP_User( $current_user->ID );
				
				
				
				$current_role = "init";
				if ( !empty( $user->roles ) && is_array( $user->roles ) ) {
				    foreach ( $user->roles as $role )
				    $current_role = $role;
				}
				
				?>

				@if(($role == "Contributor") || ($role == "Administrator"))
				@if($viewsw=="styles")
				<li class="active">		
				@else				
				<li>						
				@endif
                    <a href="/wp_omissions_styles">
                        <i class="pe-7s-arc"></i>
                        <p>{{trans('dashboard.styles') }}</p>
                    </a>
                </li>
				@endif
				
				@if($viewsw=="my_missions")
				<li class="active">		
				@else				
				<li>						
				@endif

                    <a href="/">
                        <i class="pe-7s-arc"></i>
                        <p>{{trans('dashboard.my_missions') }}</p>
                    </a>
                </li>
				
				
				
				

                   <li>
                       <a href="https://growthtroop.com/my-account">
                           <i class="pe-7s-user"></i>
                           <p>{{trans('dashboard.my_account') }}</p>
                       </a>
                   </li>
		

                
               
				
                <li>
                    <a href="/">
                        <i class="pe-7s-headphones"></i>
                        <p>{{trans('dashboard.need_help') }}</p>
                    </a>
                </li>
				
				
                   <li>
                       <a href="https://growthtroop.com/shop/">
                            <i class="pe-7s-rocket"></i>
                           <p>{{trans('dashboard.my_account') }}</p>
                       </a>
                   </li>
				
            </ul>
    	</div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
				
				
				
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"></a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                       
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
						
						<!--This is a comment. Comments are not displayed in the browser
						

                    	<li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-globe"></i>
                                    <b class="caret"></b>
                                    <span class="notification">5</span>
                              </a>
                              <ul class="dropdown-menu">
                                <li><a href="#">Mission 51 is readr review!</a></li>
                                <li><a href="#">Mission 52 is ready for review!</a></li>
                                <li><a href="#">Mission 53 is ready for review!</a></li>
                                <li><a href="#">Mission 54 is ready for review!</a></li>
                                <li><a href="#">Mission 55 is ready for review!</a></li>
                              </ul>
                        </li>
							
						-->

                        <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    Languages
                                    <b class="caret"></b>
                              </a>
                              <ul class="dropdown-menu">
                                <li id ="englishbtn"><a href="#">English</a></li>
                                <li id ="spanishbtn"><a href="#">Español</a></li>
                              </ul>
                        </li>
                        <li>
                            <a href="https://growthtroop.com/wp-login.php?action=logout">
                                Log out
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">
				
		        @yield('header')
		        @yield('content')
		
		     </div>
	    </div>


		        <footer class="footer">
		            <div class="container-fluid">
		                
						
						
		                <p class="copyright pull-right">
		                    &copy; 2016 <a href="https://growthtroop.com">growthtroop.com</a>, all rigths reserved
		                </p>
		            </div>
		        </footer>

		    </div>
		</div>


		</body>

		    <!--   Core JS Files   -->
			
			
		   
			<script src="/assets/js/bootstrap.min.js" type="text/javascript"></script>

			<!--  Checkbox, Radio & Switch Plugins -->
			<script src="/assets/js/bootstrap-checkbox-radio-switch.js"></script>

			<!--  Charts Plugin -->
			<script src="/assets/js/chartist.min.js"></script>

		    <!--  Notifications Plugin    -->
		    <script src="/assets/js/bootstrap-notify.js"></script>

		    <!--  Google Maps Plugin    -->
		    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

		    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
			<script src="/assets/js/light-bootstrap-dashboard.js"></script>

			<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
			<script src="/assets/js/demo.js"></script>

			<script type="text/javascript">
			
			// CSRF protection  
			$.ajaxSetup({
			        headers: {
			            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						//'X-CSRF-TOKEN': "{{csrf_token()}}";
			        }
			});
			
		    	$(document).ready(function(){
					
					
					
					$('#tags').tagsInput();
					
					$( "#spanishbtn" ).click(function() {
				        $.ajax({
				         type: "GET",
				         url: "/set_lang",
				         dataType: "json",
				         data: { lang: "es"},
				         success: function(data){
							 window.location = "/"
							 console.log(data);
				         }
				       });
					});
					
					$( "#englishbtn" ).click(function() {
				        $.ajax({
				         type: "GET",
				         url: "/set_lang",
				         dataType: "json",
				         data: { lang: "en"},
				         success: function(data){
							 window.location = "/"
							 console.log(data);
				         }
				       });
					});

		        	//demo.initChartist();
					/*
		        	$.notify({
		            	icon: 'pe-7s-arc',
		            	message: "Welcome to <b>GrowthTroop</b> - your low-cost digital marketing team."

		            },{
		                type: 'info',
		                timer: 4000
		            });
					*/

					//omissions_listeners();
	 
					
					

		    	});
	   		 
			</script>

		</html>
		
