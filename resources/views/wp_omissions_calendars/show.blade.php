@extends('layout')
@section('header')
<div class="page-header">
        <h1>Wp_omissions_calendars / Show #{{$wp_omissions_calendar->id}}</h1>
        <form action="{{ route('wp_omissions_calendars.destroy', $wp_omissions_calendar->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('wp_omissions_calendars.edit', $wp_omissions_calendar->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">ID</label>
                    <p class="form-control-static"></p>
                </div>
                <div class="form-group">
                     <label for="start_date">START_DATE</label>
                     <p class="form-control-static">{{$wp_omissions_calendar->start_date}}</p>
                </div>
                    <div class="form-group">
                     <label for="social_network">SOCIAL_NETWORK</label>
                     <p class="form-control-static">{{$wp_omissions_calendar->social_network}}</p>
                </div>
                    <div class="form-group">
                     <label for="content">CONTENT</label>
                     <p class="form-control-static">{{$wp_omissions_calendar->content}}</p>
                </div>
                    <div class="form-group">
                     <label for="link">LINK</label>
                     <p class="form-control-static">{{$wp_omissions_calendar->link}}</p>
                </div>
                    <div class="form-group">
                     <label for="file">FILE</label>
                     <p class="form-control-static">{{$wp_omissions_calendar->file}}</p>
                </div>
                    <div class="form-group">
                     <label for="omission_id">OMISSION_ID</label>
                     <p class="form-control-static">{{$wp_omissions_calendar->omission_id}}</p>
                </div>
            </form>

            <a class="btn btn-link" href="{{ route('wp_omissions_calendars.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

@endsection