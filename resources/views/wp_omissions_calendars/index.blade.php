@extends('layout')

@section('header')
    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> Wp_omissions_calendars
            <a class="btn btn-success pull-right" href="{{ route('wp_omissions_calendars.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
        </h1>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($wp_omissions_calendars->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>START_DATE</th>
                        <th>SOCIAL_NETWORK</th>
                        <th>CONTENT</th>
                        <th>LINK</th>
                        <th>FILE</th>
                        <th>OMISSION_ID</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($wp_omissions_calendars as $wp_omissions_calendar)
                            <tr>
                                <td>{{$wp_omissions_calendar->id}}</td>
                                <td>{{$wp_omissions_calendar->start_date}}</td>
                    <td>{{$wp_omissions_calendar->social_network}}</td>
                    <td>{{$wp_omissions_calendar->content}}</td>
                    <td>{{$wp_omissions_calendar->link}}</td>
                    <td>{{$wp_omissions_calendar->file}}</td>
                    <td>{{$wp_omissions_calendar->omission_id}}</td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('wp_omissions_calendars.show', $wp_omissions_calendar->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                    <a class="btn btn-xs btn-warning" href="{{ route('wp_omissions_calendars.edit', $wp_omissions_calendar->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    <form action="{{ route('wp_omissions_calendars.destroy', $wp_omissions_calendar->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $wp_omissions_calendars->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection