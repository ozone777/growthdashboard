@extends('layout')

@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-plus"></i> Wp_omissions_calendars / Create </h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('wp_omissions_calendars.store') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('start_date')) has-error @endif">
                       <label for="start_date-field">Start_date</label>
                    <input type="text" id="start_date-field" name="start_date" class="form-control" value="{{ old("start_date") }}"/>
                       @if($errors->has("start_date"))
                        <span class="help-block">{{ $errors->first("start_date") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('social_network')) has-error @endif">
                       <label for="social_network-field">Social_network</label>
                    <input type="text" id="social_network-field" name="social_network" class="form-control" value="{{ old("social_network") }}"/>
                       @if($errors->has("social_network"))
                        <span class="help-block">{{ $errors->first("social_network") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('content')) has-error @endif">
                       <label for="content-field">Content</label>
                    <textarea class="form-control" id="content-field" rows="3" name="content">{{ old("content") }}</textarea>
                       @if($errors->has("content"))
                        <span class="help-block">{{ $errors->first("content") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('link')) has-error @endif">
                       <label for="link-field">Link</label>
                    <input type="text" id="link-field" name="link" class="form-control" value="{{ old("link") }}"/>
                       @if($errors->has("link"))
                        <span class="help-block">{{ $errors->first("link") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('file')) has-error @endif">
                       <label for="file-field">File</label>
                    <textarea class="form-control" id="file-field" rows="3" name="file">{{ old("file") }}</textarea>
                       @if($errors->has("file"))
                        <span class="help-block">{{ $errors->first("file") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('omission_id')) has-error @endif">
                       <label for="omission_id-field">Omission_id</label>
                    <input type="text" id="omission_id-field" name="omission_id" class="form-control" value="{{ old("omission_id") }}"/>
                       @if($errors->has("omission_id"))
                        <span class="help-block">{{ $errors->first("omission_id") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a class="btn btn-link pull-right" href="{{ route('wp_omissions_calendars.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection