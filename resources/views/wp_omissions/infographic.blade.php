@extends('layout')

@section('header')
  
@endsection

@section('content')

  <div class="row">
         <div class="col-md-12">
			<h3>{{$product->get_title()}}</h3>
			
        </div>
    </div>
	
	@include('wp_omissions/general_fields')
	
	  <div class="row">
	         <div class="col-md-12">
				<p>Infographic title</p>
				
		<input type="text" name="infographic_title" omission_id="{{$wp_omission->id}}" db_field_name="infographic_title" value="{{$wp_omission->infographic_title}}" class = "form-control omissions">
				
 		         </div>
 		   </div>
	
	<br /><br /> 
	   
	  <div class="row">
	         <div class="col-md-12">
				<p>Examples</p>
				
         <textarea rows="7" cols="50" name ="infographic_examples" omission_id="{{$wp_omission->id}}" db_field_name="infographic_examples"  class ="form-control omissions">{{$wp_omission->infographic_examples}}</textarea>
				
	         </div>
	   </div>
	   
	   <br /><br />
	   
		  <div class="row">
		         <div class="col-md-12">
					<p>Keywords</p>
	<textarea rows="7" cols="50" name ="infographic_keywords" omission_id="{{$wp_omission->id}}" db_field_name="infographic_keywords"  class ="form-control omissions">{{$wp_omission->infographic_keywords}}</textarea>
		         </div>
		   </div>
	   
	   <br /><br />
		 
	  <div class="row">
	         <div class="col-md-12">
				<p>Guidelines</p>
   <textarea rows="7" cols="50" name ="infographic_guidelines" omission_id="{{$wp_omission->id}}" db_field_name="infographic_guidelines"  class ="form-control omissions">{{$wp_omission->infographic_guidelines}}</textarea>
	         </div>
	     </div>
	   
	   </div>
	   
	 <br /><br />
	 
	 @include('wp_omissions/_upload_form', array ('section' => 'infographic','fnumber' => '0', 'array' => $files))
	 
	 <script>
	 
	 $.uploadPreview({
	 	input_field: "#Uploadid0",
	 	preview_box: "#imagen-item-preview0",
	 	no_label: true
	 });
	 
	 $('#Uploadid0').on('change', function () {
	 	$("#imagen-item-preview0").fadeIn("slow");
	 	$('#imagen-item-text0').html($('#Uploadid0').val());		
	 });
	 
	 </script>
	 
	 <br /><br /><br />
	
@endsection