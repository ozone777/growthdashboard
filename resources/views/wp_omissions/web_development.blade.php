
@extends('layout')

@section('header')
  
@endsection

@section('content')

<div id ="blog_or_magazine_site">
	
     <br />
	 
 
 <div class="row">
		         
 	<div class="col-md-2">
														
 		@include('wp_omissions/_web_states')
											
 	</div>
				
 </div>
	
		<br />

	    <div class="row">
		
	         <div class="col-md-3">
		    
			
			 </div>
		
	           <div class="col-md-2" style="text-align:right;">
				
				   	 <div class="circlep" style="background: #9ea6a4;"><a href="/web/{{$wp_omission->id}}">Initial information</a></div>
			  
	          </div>
	  
      
	             <div class="col-md-2">
			   			   
	  			   <div class="circlep" style="background: #54d4ad;"><a href="/web_development/{{$wp_omission->id}}">Web development</a></div>
  			
	            </div>
			
			
	        </div>
		
	        <div class="row">
	               <div class="col-md-6">
			   			   
	    			    <h3>{{$product->get_title()}}</h3>
  			
	              </div>
	          </div>
		
		
	    <div class="row">
	           <div class="col-md-12">
				   
		
				<input type="text" name="web_design_section1_title" omission_id="{{$wp_omission->id}}" db_field_name="web_design_section1_title" value="{{$wp_omission->web_design_section1_title}}" class = "form-control omissions" placeholder= "Section 1 title">
				
				<br />
				
				<textarea rows="7" cols="50" name ="web_design_section1_content" omission_id="{{$wp_omission->id}}" db_field_name="web_design_section1_content" placeholder= "Section 1 content"  class ="form-control omissions">{{$wp_omission->web_design_section1_content}}</textarea>
				
	           </div>
	     </div>
		 
		 
		 <br />
		 
		
		@include('wp_omissions/_upload_form', array ('section' => 'section1','fnumber' => '1', 'array' => $files, 'updated_view' => 'web_development'))
		 
		 <br /><br />
		 
 	    <div class="row">
 	           <div class="col-md-12">
				
				<input type="text" placeholder= "Section 2 title" name="web_design_section2_title" omission_id="{{$wp_omission->id}}" db_field_name="web_design_section2_title" value="{{$wp_omission->web_design_section2_title}}" class = "form-control omissions">
				
				<br />
			
				<textarea rows="7" cols="50" placeholder= "Section 2 content"  name ="web_design_section2_content" omission_id="{{$wp_omission->id}}" db_field_name="web_design_section2_content"  class ="form-control omissions">{{$wp_omission->web_design_section2_content}}</textarea>
		
 	           </div>
 	     </div>
		 
		 <br />
		 
 	    @include('wp_omissions/_upload_form', array ('section' => 'section2','fnumber' => '2', 'array' => $files, 'updated_view' => 'web_development'))
		 
 		 <br /><br />
		 
		 
 	    <div class="row">
 	           <div class="col-md-12">
				
				<input type="text" placeholder= "Section 3 title" name="web_design_section3_title" omission_id="{{$wp_omission->id}}" db_field_name="web_design_section3_title" value="{{$wp_omission->web_design_section3_title}}" class = "form-control omissions">
				
				<br />
			
				<textarea rows="7" cols="50" placeholder= "Section 3 content"  name ="web_design_section3_content" omission_id="{{$wp_omission->id}}" db_field_name="web_design_section3_content"  class ="form-control omissions">{{$wp_omission->web_design_section3_content}}</textarea>
		
 	           </div>
 	     </div>
		 
		 <br />
		 
  	    @include('wp_omissions/_upload_form', array ('section' => 'section3','fnumber' => '3', 'array' => $files, 'updated_view' => 'web_development'))
		 
 		 <br /><br />
		 
		 
 	    <div class="row">
 	           <div class="col-md-12">
 	
				
				<input type="text" placeholder= "Section 4 title" name="web_design_section4_title" omission_id="{{$wp_omission->id}}" db_field_name="web_design_section4_title" value="{{$wp_omission->web_design_section4_title}}" class = "form-control omissions">
				
				<br />
						
				<textarea rows="7" cols="50" placeholder= "Section 4 content"  name ="web_design_section4_content" omission_id="{{$wp_omission->id}}" db_field_name="web_design_section4_content"  class ="form-control omissions">{{$wp_omission->web_design_section4_content}}</textarea>
		
 	           </div>
 	     </div>
		 
		 <br />
		 
   	   @include('wp_omissions/_upload_form', array ('section' => 'section4','fnumber' => '4', 'array' => $files, 'updated_view' => 'web_development'))
		 
 		 <br /><br />
		 
		 
 	    <div class="row">
 	           <div class="col-md-12">

				
				<input type="text" placeholder= "Section 5 title" name="web_design_section5_title" omission_id="{{$wp_omission->id}}" db_field_name="web_design_section5_title" value="{{$wp_omission->web_design_section5_title}}" class = "form-control omissions">
				
				<br />
				
				<textarea rows="7" cols="50" placeholder= "Section 5 content"  name ="web_design_section5_content" omission_id="{{$wp_omission->id}}" db_field_name="web_design_section5_content"  class ="form-control omissions">{{$wp_omission->web_design_section5_content}}</textarea> 
		
 	           </div>
 	     </div>
		 
		 <br />
		 
    	  @include('wp_omissions/_upload_form', array ('section' => 'section5','fnumber' => '5', 'array' => $files, 'updated_view' => 'web_development'))
		 
 		 <br /><br />
		 
		 
 	    <div class="row">
 	           <div class="col-md-12">
				
				<input type="text" placeholder= "Section 6 title" name="web_design_section6_title" omission_id="{{$wp_omission->id}}" db_field_name="web_design_section6_title" value="{{$wp_omission->web_design_section6_title}}" class = "form-control omissions">
				
				<br />
				
				<textarea rows="7" cols="50" placeholder= "Section 6 content"  name ="web_design_section6_content" omission_id="{{$wp_omission->id}}" db_field_name="web_design_section6_content"  class ="form-control omissions">{{$wp_omission->web_design_section6_content}}</textarea> 
		
 	           </div>
 	     </div>
		 
		 <br />
		 
 	    @include('wp_omissions/_upload_form', array ('section' => 'section6','fnumber' => '6', 'array' => $files, 'updated_view' => 'web_development'))
		
 		 <br /><br />
		 
		 
 	    <div class="row">
 	           <div class="col-md-12">

				<input type="text" placeholder= "Section 7 title" name="web_design_section7_title" omission_id="{{$wp_omission->id}}" db_field_name="web_design_section7_title" value="{{$wp_omission->web_design_section7_title}}" class = "form-control omissions">
				
				<br />
				
				<textarea rows="7" cols="50" placeholder= "Section 7 content"  name ="web_design_section7_content" omission_id="{{$wp_omission->id}}" db_field_name="web_design_section7_content"  class ="form-control omissions">{{$wp_omission->web_design_section7_content}}</textarea> 
		
 	           </div>
 	     </div>
		 
		 <br />
		 
  	   @include('wp_omissions/_upload_form', array ('section' => 'section7','fnumber' => '7', 'array' => $files, 'updated_view' => 'web_development'))
	     <br /><br />
	 
</div>

@include('wp_omissions/js_logic')

<script>

$( document ).ready(function() {
	
	 omissions_listeners();
	 //upload_logic();
	 //delete_file_logic();
	 
	//$("#gallery").unitegallery();
	//$("#web_styles_gallery").unitegallery();
	
	
    
});

</script>

@endsection
