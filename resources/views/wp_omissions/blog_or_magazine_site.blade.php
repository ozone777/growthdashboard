<div id ="blog_or_magazine_site">
	
	
	
	
  <div class="row">
         <div class="col-md-6">
			<p>Site name</p>
	
			{!!Form::text('web_design_domain',$wp_omission->web_design_domain,['class' => 'form-control'])!!} 
	
         </div>
	 
         <div class="col-md-6">
			<p>Contact form email account</p>
	
			{!!Form::text('web_design_contact_form_email',$wp_omission->web_design_contact_form_email,['class' => 'form-control'])!!} 
	
         </div>
	 
   </div>
   
   <br />
   
   <div class="row">
          <div class="col-md-6">
 			<p>Web site admin username</p>
		
 			{!!Form::text('web_design_admin_username',$wp_omission->web_design_admin_username,['class' => 'form-control'])!!} 
		
          </div>
	 
          <div class="col-md-6">
 			<p>Web site admin password <i>(This data will be encrypted)<i></p>
		
 			{!!Form::text('web_design_admin_desired_password',$wp_omission->web_design_admin_desired_password,['class' => 'form-control'])!!} 
		
          </div>
    </div>   
 
     <br />
  
    <div class="row">
	
           <div class="col-md-6">
  			<p>Company address</p>
	
  			{!!Form::textarea('company_address',$wp_omission->company_address,['class' => 'form-control'])!!} 
	
           </div>
	   
           <div class="col-md-6">
  			<p>Company phones</p>
	
  			{!!Form::textarea('company_phones',$wp_omission->company_phones,['class' => 'form-control'])!!} 
	
           </div>
	   
     </div>
  <br />
	  
	  
      <div class="row">
		
             <div class="col-md-6">
    			<p>Company portfolio</p>
		
    			{!!Form::textarea('company_portfolio',$wp_omission->company_portfolio,['class' => 'form-control'])!!} 
		
             </div>
		   
             <div class="col-md-6">
    			<p>5 Web site examples</p>
		
    			{!!Form::textarea('examples',$wp_omission->examples,['class' => 'form-control'])!!} 
		
             </div>
		   
       </div>
	  <br />
	  
      <div class="row">
		
             <div class="col-md-6">
    			<p>Company domain keys <i>(This data will be encrypted)<i></p>
		
    			{!!Form::textarea('company_domain_keys',$wp_omission->company_domain_keys,['class' => 'form-control'])!!} 
		
             </div>
		   
             <div class="col-md-6">
    			<p>Social networks URL</p>
		
    			{!!Form::textarea('web_design_social_urls',$wp_omission->web_design_social_urls,['class' => 'form-control'])!!} 
		
             </div>
		   
       </div>
	  <br />
	 

    <br /><br />
   

	
	    <div class="row">
	           <div class="col-md-12">
	  			<p>Logo files</p>
				
	   		 @foreach($files as $file)
		 	
	   			@if($file->section == "logosection")
			
	   			@if($file->title)
	   			<li>
	   			<a href="/files/{{$wp_omission->id}}/{{$file->file}}">>{{$file->title}}</a>
	   			&nbsp;&nbsp;&nbsp;
	   			<a href="/delete_file/{{$file->id}}">Delete</a>
	   			</li>
	   			@else				
	   			<li>
	   			<a href="/files/{{$wp_omission->id}}/{{$file->file}}">>File</a>
	   			&nbsp;&nbsp;&nbsp;
	   			<a href="/delete_file/{{$file->id}}">Delete</a>
	   			</li>			
	   			@endif
			
	   			@endif
		   
	   		 @endforeach
				
	          </div>
	    </div>
		
		<br />
	
	    <div class="row">
			
 	           <div class="col-md-6">
                <input type="text" id="logotitle1-field" name="logotitle1" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
           </div>
      

        <div class="col-md-6">
		
              <input type="file" id="logofile1-field" name="logofile1">
        </div>

		 </div>
		 
	    <div class="row">
			
 	           <div class="col-md-6">
                <input type="text" id="logotitle2-field" name="logotitle2" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
	           </div>
      

        <div class="col-md-6">
		
               <input type="file" id="logofile2-field" name="logofile2">
        </div>

		 </div>
		 
	    <div class="row">
			
 	           <div class="col-md-6">
                <input type="text" id="logotitle3-field" name="logotitle3" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
	           </div>
      

        <div class="col-md-6">
		
               <input type="file" id="logofile3-field" name="logofile3">
        </div>

		 </div>
		 
		 
	    <div class="row">
 	           <div class="col-md-6">
                <input type="text" id="logotitle4-field" name="logotitle4" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
 	           </div>
      

        <div class="col-md-6">
		
                <input type="file" id="logofile4-field" name="logofile4">
        </div>

		 </div>
		 
	    <div class="row">
 	           <div class="col-md-6">
                <input type="text" id="logotitle5-field" name="logotitle5" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
  	           </div>
      

	        <div class="col-md-6">
		
                 <input type="file" id="logofile5-field" name="logofile5">
        </div>

		 </div>
		 
		 <br />
		 
		<div class="row">
        <div class="col-md-6">
              <button type="submit" class="form-control button-next btn btn-info addfiles" section="logosection">Add files</button>
        </div>
		</div>
		 
		 
		<br />

		
	    <div class="row">
	           <div class="col-md-12">
	  			<p>Section 1 title</p>
		
	  			{!!Form::text('web_design_section1_title',$wp_omission->web_design_section1_title,['class' => 'form-control'])!!} 
				<br />
				<p>Section 1 content</p>
				{!!Form::textarea('web_design_section1_content',$wp_omission->web_design_section1_content,['class' => 'form-control ckeditor_area'])!!}
				
	           </div>
	     </div>
		 
		 
		 <br />
		 
		 <ul>
		 @foreach($files as $file)
		 	
			@if($file->section == "section1")
			
			@if($file->title)
			<li>
			<a href="/files/{{$wp_omission->id}}/{{$file->file}}">>{{$file->title}}</a>
			&nbsp;&nbsp;&nbsp;
			<a href="/delete_file/{{$file->id}}">Delete</a>
			</li>
			@else				
			<li>
			<a href="/files/{{$wp_omission->id}}/{{$file->file}}">>File</a>
			&nbsp;&nbsp;&nbsp;
			<a href="/delete_file/{{$file->id}}">Delete</a>
			</li>			
			@endif
			
			@endif
		   
		 @endforeach
	     </ul>
		    <div class="row">
	 	           <div class="col-md-6">
	                <input type="text" id="title1-field" name="title1" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
   	           </div>
   	      

 	        <div class="col-md-6">
			
                  <input type="file" id="file1-field" name="file1">
	        </div>

			 </div>
			 
 		    <div class="row">
 	 	           <div class="col-md-6">
 	                <input type="text" id="title2-field" name="title2" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
    	           </div>
   	      

  	        <div class="col-md-6">
			
                   <input type="file" id="file2-field" name="file2">
 	        </div>

 			 </div>
			 
 		    <div class="row">
 	 	           <div class="col-md-6">
 	                <input type="text" id="title3-field" name="title3" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
    	           </div>
   	      

  	        <div class="col-md-6">
			
                   <input type="file" id="file3-field" name="file3">
 	        </div>

 			 </div>
			 
			 
  		    <div class="row">
  	 	           <div class="col-md-6">
  	                <input type="text" id="title4-field" name="title4" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
     	           </div>
   	      

   	        <div class="col-md-6">
			
                    <input type="file" id="file4-field" name="file4">
  	        </div>

  			 </div>
			 
   		    <div class="row">
   	 	           <div class="col-md-6">
   	                <input type="text" id="title5-field" name="title5" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
      	           </div>
   	      

    	        <div class="col-md-6">
			
                     <input type="file" id="file5-field" name="file5">
   	        </div>

   			 </div>
			 
			 
			<br /> 
			 
			<div class="row">
  	        <div class="col-md-6">
 	              <button type="submit" class="form-control button-next btn btn-info addfiles" section="section1">Add files</button>
 	        </div>
			</div>
            
		 
		 
		 <br /><br />
		 
 	    <div class="row">
 	           <div class="col-md-12">
 	  			<p>Section 2 title</p>
		
 	  			{!!Form::text('web_design_section2_title',$wp_omission->web_design_section2_title,['class' => 'form-control'])!!} 
				
				<br />
				<p>Section 2 content</p>
				{!!Form::textarea('web_design_section2_content',$wp_omission->web_design_section2_content,['class' => 'form-control ckeditor_area'])!!}
		
 	           </div>
 	     </div>
		 
		 <br />
		 
		 <ul>
		 @foreach($files as $file)
		 	
			@if($file->section == "section2")
			
			@if($file->title)
			<li>
			<a href="/files/{{$wp_omission->id}}/{{$file->file}}">{{$file->title}}</a>
			&nbsp;&nbsp;&nbsp;
			<a href="/delete_file/{{$file->id}}">Delete</a>
			</li>
			@else				
			<li>
			<a href="/files/{{$wp_omission->id}}/{{$file->file}}">File</a>
			&nbsp;&nbsp;&nbsp;
			<a href="/delete_file/{{$file->id}}">Delete</a>
			</li>			
			@endif
			
			@endif
		   
		 @endforeach
	     </ul>
		 
		 <br />
		  
		    <div class="row">
	 	           <div class="col-md-6">
	                <input type="text" id="title6-field" name="title6" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
   	           </div>
   	      

 	        <div class="col-md-6">
			
                  <input type="file" id="file6-field" name="file6">
	        </div>

			 </div>
			 
 		    <div class="row">
 	 	           <div class="col-md-6">
 	                <input type="text" id="title7-field" name="title7" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
    	           </div>
   	      

  	        <div class="col-md-6">
			
                   <input type="file" id="file7-field" name="file7">
 	        </div>

 			 </div>
			 
 		    <div class="row">
 	 	           <div class="col-md-6">
 	                <input type="text" id="title8-field" name="title8" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
    	           </div>
   	      

  	        <div class="col-md-6">
			
                   <input type="file" id="file8-field" name="file8">
 	        </div>

 			 </div>
			 
			 
  		    <div class="row">
  	 	           <div class="col-md-6">
  	                <input type="text" id="title9-field" name="title9" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
     	           </div>
   	      

   	        <div class="col-md-6">
			
                    <input type="file" id="file9-field" name="file9">
  	        </div>

  			 </div>
			 
   		    <div class="row">
   	 	           <div class="col-md-6">
   	                <input type="text" id="title10-field" name="title10" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
      	           </div>
   	      

    	        <div class="col-md-6">
			
                     <input type="file" id="file10-field" name="file10">
   	        </div>

   			 </div>
			 
			 
			 <br />
			 
			<div class="row">
  	        <div class="col-md-6">
 	              <button type="submit" class="form-control button-next btn btn-info addfiles" section="section2">Add files</button>
 	        </div>
			</div>
		 
 		 <br /><br />
		 
		 
 	    <div class="row">
 	           <div class="col-md-12">
 	  			<p>Section 3 title</p>
		
 	  			{!!Form::text('web_design_section3_title',$wp_omission->web_design_section3_title,['class' => 'form-control'])!!} 
				
				<br />
				<p>Section 3 content</p>
				{!!Form::textarea('web_design_section3_content',$wp_omission->web_design_section3_content,['class' => 'form-control ckeditor_area'])!!}
		
 	           </div>
 	     </div>
		 
		 <br />
		 
		 <ul>
		 @foreach($files as $file)
		 	
			@if($file->section == "section3")
			
			@if($file->title)
			<li>
			<a href="/files/{{$wp_omission->id}}/{{$file->file}}">{{$file->title}}</a>
			&nbsp;&nbsp;&nbsp;
			<a href="/delete_file/{{$file->id}}">Delete</a>
			</li>
			@else				
			<li>
			<a href="/files/{{$wp_omission->id}}/{{$file->file}}">File</a>
			&nbsp;&nbsp;&nbsp;
			<a href="/delete_file/{{$file->id}}">Delete</a>
			</li>			
			@endif
			
			@endif
		   
		 @endforeach
	     </ul>
		 
		 <br />
		  
		    <div class="row">
	 	           <div class="col-md-6">
	                <input type="text" id="title11-field" name="title11" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
   	           </div>
   	      

 	        <div class="col-md-6">
			
                  <input type="file" id="file11-field" name="file11">
	        </div>

			 </div>
			 
 		    <div class="row">
 	 	           <div class="col-md-6">
 	                <input type="text" id="title12-field" name="title12" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
    	           </div>
   	      

  	        <div class="col-md-6">
			
                   <input type="file" id="file12-field" name="file12">
 	        </div>

 			 </div>
			 
 		    <div class="row">
 	 	           <div class="col-md-6">
 	                <input type="text" id="title13-field" name="title13" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
    	           </div>
   	      

  	        <div class="col-md-6">
			
                   <input type="file" id="file13-field" name="file13">
 	        </div>

 			 </div>
			 
			 
  		    <div class="row">
  	 	           <div class="col-md-6">
  	                <input type="text" id="title14-field" name="title14" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
     	           </div>
   	      

   	        <div class="col-md-6">
			
                    <input type="file" id="file14-field" name="file14">
  	        </div>

  			 </div>
			 
   		    <div class="row">
   	 	           <div class="col-md-6">
   	                <input type="text" id="title15-field" name="title15" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
      	           </div>
   	      

    	        <div class="col-md-6">
			
                     <input type="file" id="file15-field" name="file15">
   	        </div>

   			 </div>
			 
			 <br />
			 
			 
			<div class="row">
  	        <div class="col-md-6">
 	              <button type="submit" class="form-control button-next btn btn-info addfiles" section="section3">Add files</button>
 	        </div>
			</div>
		 
 		 <br /><br />
		 
		 
 	    <div class="row">
 	           <div class="col-md-12">
 	  			<p>Section 4 title</p>
		
 	  			{!!Form::text('web_design_section4_title',$wp_omission->web_design_section4_title,['class' => 'form-control'])!!} 
				
				<br />
				<p>Section 4 content</p>
				{!!Form::textarea('web_design_section4_content',$wp_omission->web_design_section4_content,['class' => 'form-control ckeditor_area'])!!}
		
 	           </div>
 	     </div>
		 
		 <br />
		 
		 <ul>
		 @foreach($files as $file)
		 	
			@if($file->section == "section4")
			
			@if($file->title)
			<li>
			<a href="/files/{{$wp_omission->id}}/{{$file->file}}">{{$file->title}}</a>
			&nbsp;&nbsp;&nbsp;
			<a href="/delete_file/{{$file->id}}">Delete</a>
			</li>
			@else				
			<li>
			<a href="/files/{{$wp_omission->id}}/{{$file->file}}">File</a>
			&nbsp;&nbsp;&nbsp;
			<a href="/delete_file/{{$file->id}}">Delete</a>
			</li>			
			@endif
			
			@endif
		   
		 @endforeach
	     </ul>
		 
		 <br />
		  
		    <div class="row">
	 	           <div class="col-md-6">
	                <input type="text" id="title16-field" name="title16" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
   	           </div>
   	      

 	        <div class="col-md-6">
			
                  <input type="file" id="file16-field" name="file16">
	        </div>

			 </div>
			 
 		    <div class="row">
 	 	           <div class="col-md-6">
 	                <input type="text" id="title17-field" name="title17" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
    	           </div>
   	      

  	        <div class="col-md-6">
			
                   <input type="file" id="file17-field" name="file17">
 	        </div>

 			 </div>
			 
 		    <div class="row">
 	 	           <div class="col-md-6">
 	                <input type="text" id="title18-field" name="title18" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
    	           </div>
   	      

  	        <div class="col-md-6">
			
                   <input type="file" id="file18-field" name="file18">
 	        </div>

 			 </div>
			 
			 
  		    <div class="row">
  	 	           <div class="col-md-6">
  	                <input type="text" id="title19-field" name="title19" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
     	           </div>
   	      

   	        <div class="col-md-6">
			
                    <input type="file" id="file19-field" name="file19">
  	        </div>

  			 </div>
			 
   		    <div class="row">
   	 	           <div class="col-md-6">
   	                <input type="text" id="title20-field" name="title20" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
      	           </div>
   	      

    	        <div class="col-md-6">
			
                     <input type="file" id="file20-field" name="file20">
   	        </div>

   			 </div>
			 
			 <br />
			 
			 
			<div class="row">
  	        <div class="col-md-6">
 	              <button type="submit" class="form-control button-next btn btn-info addfiles" section="section4">Add files</button>
 	        </div>
			</div>
		 
 		 <br /><br />
		 
		 
 	    <div class="row">
 	           <div class="col-md-12">
 	  			<p>Section 5 title</p>
		
 	  			{!!Form::text('web_design_section5_title',$wp_omission->web_design_section5_title,['class' => 'form-control'])!!}
				
				<br />
				<p>Section 5 content</p>
				{!!Form::textarea('web_design_section5_content',$wp_omission->web_design_section5_content,['class' => 'form-control ckeditor_area'])!!} 
		
 	           </div>
 	     </div>
		 
		 <br />
		 
		 <ul>
		 @foreach($files as $file)
		 	
			@if($file->section == "section5")
			
			@if($file->title)
			<li>
			<a href="/files/{{$wp_omission->id}}/{{$file->file}}">{{$file->title}}</a>
			&nbsp;&nbsp;&nbsp;
			<a href="/delete_file/{{$file->id}}">Delete</a>
			</li>
			@else				
			<li>
			<a href="/files/{{$wp_omission->id}}/{{$file->file}}">File</a>
			&nbsp;&nbsp;&nbsp;
			<a href="/delete_file/{{$file->id}}">Delete</a>
			</li>			
			@endif
			
			@endif
		   
		 @endforeach
	     </ul>
		 
		 <br />
		  
		    <div class="row">
	 	           <div class="col-md-6">
	                <input type="text" id="title21-field" name="title21" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
   	           </div>
   	      

 	        <div class="col-md-6">
			
                  <input type="file" id="file21-field" name="file21">
	        </div>

			 </div>
			 
 		    <div class="row">
 	 	           <div class="col-md-6">
 	                <input type="text" id="title22-field" name="title22" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
    	           </div>
   	      

  	        <div class="col-md-6">
			
                   <input type="file" id="file22-field" name="file22">
 	        </div>

 			 </div>
			 
 		    <div class="row">
 	 	           <div class="col-md-6">
 	                <input type="text" id="title23-field" name="title23" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
    	           </div>
   	      

  	        <div class="col-md-6">
			
                   <input type="file" id="file23-field" name="file23">
 	        </div>

 			 </div>
			 
			 
  		    <div class="row">
  	 	           <div class="col-md-6">
  	                <input type="text" id="title24-field" name="title24" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
     	           </div>
   	      

   	        <div class="col-md-6">
			
                    <input type="file" id="file24-field" name="file24">
  	        </div>

  			 </div>
			 
   		    <div class="row">
   	 	           <div class="col-md-6">
   	                <input type="text" id="title25-field" name="title25" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
      	           </div>
   	      

    	        <div class="col-md-6">
			
                     <input type="file" id="file25-field" name="file25">
   	        </div>

   			 </div>
			 
			 
			 <br />
			 
			<div class="row">
  	        <div class="col-md-6">
 	              <button type="submit" class="form-control button-next btn btn-info addfiles" section="section5">Add files</button>
 	        </div>
			</div>
		 
 		 <br /><br />
		 
		 
 	    <div class="row">
 	           <div class="col-md-12">
 	  			<p>Section 6 title</p>
		
 	  			{!!Form::text('web_design_section6_title',$wp_omission->web_design_section6_title,['class' => 'form-control'])!!}
				
				<br />
				<p>Section 6 content</p>
				{!!Form::textarea('web_design_section6_content',$wp_omission->web_design_section6_content,['class' => 'form-control ckeditor_area'])!!} 
		
 	           </div>
 	     </div>
		 
		 <br />
		 
		 <ul>
		 @foreach($files as $file)
		 	
			@if($file->section == "section6")
			
			@if($file->title)
			<li>
			<a href="/files/{{$wp_omission->id}}/{{$file->file}}">{{$file->title}}</a>
			&nbsp;&nbsp;&nbsp;
			<a href="/delete_file/{{$file->id}}">Delete</a>
			</li>
			@else				
			<li>
			<a href="/files/{{$wp_omission->id}}/{{$file->file}}">File</a>
			&nbsp;&nbsp;&nbsp;
			<a href="/delete_file/{{$file->id}}">Delete</a>
			</li>			
			@endif
			
			@endif
		   
		 @endforeach
	     </ul>
		 
		 <br />
		  
		    <div class="row">
	 	           <div class="col-md-6">
	                <input type="text" id="title26-field" name="title26" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
   	           </div>
   	      

 	        <div class="col-md-6">
			
                  <input type="file" id="file26-field" name="file26">
	        </div>

			 </div>
			 
 		    <div class="row">
 	 	           <div class="col-md-6">
 	                <input type="text" id="title27-field" name="title27" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
    	           </div>
   	      

  	        <div class="col-md-6">
			
                   <input type="file" id="file27-field" name="file27">
 	        </div>

 			 </div>
			 
 		    <div class="row">
 	 	           <div class="col-md-6">
 	                <input type="text" id="title28-field" name="title28" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
    	           </div>
   	      

  	        <div class="col-md-6">
			
                   <input type="file" id="file28-field" name="file28">
 	        </div>

 			 </div>
			 
			 
  		    <div class="row">
  	 	           <div class="col-md-6">
  	                <input type="text" id="title29-field" name="title29" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
     	           </div>
   	      

   	        <div class="col-md-6">
			
                    <input type="file" id="file29-field" name="file29">
  	        </div>

  			 </div>
			 
   		    <div class="row">
   	 	           <div class="col-md-6">
   	                <input type="text" id="title30-field" name="title30" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
      	           </div>
   	      

    	        <div class="col-md-6">
			
                     <input type="file" id="file30-field" name="file30">
   	        </div>

   			 </div>
			 
			 
			 <br />
			 
			
			 
			<div class="row">
  	        <div class="col-md-6">
 	              <button type="submit" class="form-control button-next btn btn-info addfiles" section="section6">Add files</button>
 	        </div>
			</div>
		 
 		 <br /><br />
		 
		 
 	    <div class="row">
 	           <div class="col-md-12">
 	  			<p>Section 7 title</p>
		
 	  			{!!Form::text('web_design_section7_title',$wp_omission->web_design_section7_title,['class' => 'form-control'])!!} 
				
				<br />
				<p>Section 7 content</p>
				{!!Form::textarea('web_design_section7_content',$wp_omission->web_design_section7_content,['class' => 'form-control ckeditor_area'])!!}
		
 	           </div>
 	     </div>
		 
		 <br />
		 
		 <ul>
		 @foreach($files as $file)
		 	
			@if($file->section == "section7")
			
			@if($file->title)
			<li>
			<a href="/files/{{$wp_omission->id}}/{{$file->file}}">{{$file->title}}</a>
			&nbsp;&nbsp;&nbsp;
			<a href="/delete_file/{{$file->id}}">Delete</a>
			</li>
			@else				
			<li>
			<a href="/files/{{$wp_omission->id}}/{{$file->file}}">File</a>
			&nbsp;&nbsp;&nbsp;
			<a href="/delete_file/{{$file->id}}">Delete</a>
			</li>			
			@endif
			
			@endif
		   
		 @endforeach
	     </ul>
		 
		 <br />
		  
		    <div class="row">
	 	           <div class="col-md-6">
	                <input type="text" id="title31-field" name="title31" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
   	           </div>
   	      

 	        <div class="col-md-6">
			
                  <input type="file" id="file31-field" name="file31">
	        </div>

			 </div>
			 
 		    <div class="row">
 	 	           <div class="col-md-6">
 	                <input type="text" id="title32-field" name="title32" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
    	           </div>
   	      

  	        <div class="col-md-6">
			
                   <input type="file" id="file32-field" name="file32">
 	        </div>

 			 </div>
			 
 		    <div class="row">
 	 	           <div class="col-md-6">
 	                <input type="text" id="title33-field" name="title33" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
    	           </div>
   	      

  	        <div class="col-md-6">
			
                   <input type="file" id="file33-field" name="file33">
 	        </div>

 			 </div>
			 
			 
  		    <div class="row">
  	 	           <div class="col-md-6">
  	                <input type="text" id="title34-field" name="title34" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
     	           </div>
   	      

   	        <div class="col-md-6">
			
                    <input type="file" id="file34-field" name="file34">
  	        </div>

  			 </div>
			 
   		    <div class="row">
   	 	           <div class="col-md-6">
   	                <input type="text" id="title35-field" name="title35" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
      	           </div>
   	      

    	        <div class="col-md-6">
			
                     <input type="file" id="file35-field" name="file35">
   	        </div>

   			 </div>
			 
			 <br />
			 
			 
			 
			 
			<div class="row">
  	        <div class="col-md-6">
 	              <button type="submit" class="form-control button-next btn btn-info addfiles" section="section7">Add files</button>
 	        </div>
			</div>
		 
		 <br /><br />
		 
	     <div class="row">
	         <div class="col-md-12">
                <button type="submit" class="form-control button-next  btn btn-info">Save</button>
	         </div>
	     </div>
		 
	     <br /><br />
		 
		 
		 
 	      <div class="row">
 	             <div class="col-md-12">
 	    			<h2>Web style gallery</h2>
			
			
 	  			<div id="web_styles_gallery" style="display:none;">
			
 	     		 @foreach($wp_omissions_styles as $style)
	 			 
				 @if($style->file)
				 
 	  			<img alt="{{$style->es}}"
 	  				 src="/styles/{{$style->file}}"
 	  				 data-image="/styles/{{$style->file}}"
 	  				 data-description="{{$style->es}}">
					 
				@endif
	
 	     		 @endforeach
		 
 	  		 </div>
			
 	            </div>
 	      </div>
	
 	  	<br />
		
	    <div class="row">
	        <div class="col-md-12">
				@foreach($wp_omissions_styles as $style)
				
				
				<div class ="col-md-4">
					
					@if(in_array($style->id, $styles_ids_array))
					<input type="checkbox" name="webstlyes[]" value="{{$style->id}}" checked>{{$style->es}}<br />
					@else				
					<input type="checkbox" name="webstlyes[]" value="{{$style->id}}">{{$style->es}}<br />			
					@endif
				
					
					
		 		</div>
				
				@endforeach
	            </div>
	      </div>
		 

	      <div class="row">
	             <div class="col-md-12">
	    			<h2>Mockups Gallery</h2>
			
			
	  			<div id="gallery" style="display:none;">
	
			
	 		
			
			
	     		 @foreach($files as $file)
	 	
	     			@if($file->section == "mockups_section")
		
				
			 	
		
	     			@if($file->title)
   			
			
	  			<img alt="Preview Image 1"
	  				 src="/files/{{$file->file}}"
	  				 data-image="/files/{{$file->file}}"
	  				 data-description="{{$file->title}}">
			
	     			@else				

	  			<img alt="Preview Image 1"
	  				 src="/files/{{$file->file}}"
	  				 data-image="/files/{{$file->file}}"
	  				 data-description="">
				
	     			@endif
		
	     			@endif
	   
	     		 @endforeach
		 
	  		 </div>
			
	            </div>
	      </div>
	
	  	<br />

	      <div class="row">
		
	  	           <div class="col-md-6">
	                 <input type="text" id="mockuptitle1-field" name="mockuptitle1" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
	            </div>
     

	         <div class="col-md-6">
	
	               <input type="file" id="mockupfile1-field" name="mockupfile1">
	         </div>

	  	 </div>
	 
	      <div class="row">
		
	  	           <div class="col-md-6">
	                 <input type="text" id="mockuptitle2-field" name="mockuptitle2" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
	             </div>
     

	         <div class="col-md-6">
	
	                <input type="file" id="mockupfile2-field" name="mockupfile2">
	         </div>

	  	 </div>
	 
	      <div class="row">
		
	  	           <div class="col-md-6">
	                 <input type="text" id="mockuptitle3-field" name="mockuptitle3" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
	             </div>
     

	         <div class="col-md-6">
	
	                <input type="file" id="mockupfile3-field" name="mockupfile3">
	         </div>

	  	 </div>
	 
	 
	      <div class="row">
	  	           <div class="col-md-6">
	                 <input type="text" id="mockuptitle4-field" name="mockuptitle4" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
	  	           </div>
     

	         <div class="col-md-6">
	
	                 <input type="file" id="mockupfile4-field" name="logofile4">
	         </div>

	  	 </div>
	 
	      <div class="row">
	  	           <div class="col-md-6">
	                 <input type="text" id="mockuptitle5-field" name="mockuptitle5" placeholder="File label" class="form-control" value="{{ old("title") }}"/>
	   	           </div>
     

	          <div class="col-md-6">
	
	                  <input type="file" id="mockupfile5-field" name="mockupfile5">
	         </div>

	  	 </div>
	 
	  	 <br />
	 
	  	<div class="row">
	         <div class="col-md-6">
	               <button type="submit" class="form-control button-next btn btn-info addfiles" section="mockups_section">Add files</button>
	         </div>
	  	</div>
	 
	 
	  	<br />
		 
		 
		 
		 <br /><br />
		 
	 
</div>

<script>

$( document ).ready(function() {
		
	$("#gallery").unitegallery();
	$("#web_styles_gallery").unitegallery();
    
});

</script>
