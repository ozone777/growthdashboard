@extends('layout')

@section('header')
  
@endsection

@section('content')


  <div class="row">
         <div class="col-md-12">
			<h3>{{$product->get_title()}}</h3>
			
        </div>
    </div>

			  <div id ="article_writing">
				  
				  @include('wp_omissions/general_fields')
				  
       			  <div class="row">
       			         <div class="col-md-12">
       						<p>Blog title</p>
							
					<input type="text" name="blog_title" omission_id="{{$wp_omission->id}}" db_field_name="blog_title" value="{{$wp_omission->blog_title}}" class = "form-control omissions">
							
          		         </div>
          		   </div>
			 <br /><br />
 			  <div class="row">
 			         <div class="col-md-2">
						
							
							<input name="blog_goal_improve_seo" @if($wp_omission->blog_goal_improve_seo == 1)checked= 'checked' @endif db_field_name="blog_goal_improve_seo" value="1" class="omissions_check" omission_id="{{$wp_omission->id}}"  type="checkbox">  Improve Seo
								
    		         </div>
					 
 			         <div class="col-md-2">
						
						<input name="blog_goal_become_a_thought_leader" @if($wp_omission->blog_goal_become_a_thought_leader == 1)checked= 'checked' @endif db_field_name="blog_goal_become_a_thought_leader" value="1" class="omissions_check" omission_id="{{$wp_omission->id}}" type="checkbox">  Become thought leader
						
    		         </div>
					 
 			         <div class="col-md-2">
						
						<input name="blog_goal_recycle_existing_content" @if($wp_omission->blog_goal_recycle_existing_content == 1)checked= 'checked' @endif db_field_name="blog_goal_recycle_existing_content" value="1" class="omissions_check" omission_id="{{$wp_omission->id}}" type="checkbox">  Become thought leader
						 
    		         </div>
					 
 			         <div class="col-md-2">
						 
						 <input name="blog_goal_provide_instructions" @if($wp_omission->blog_goal_provide_instructions == 1)checked= 'checked' @endif db_field_name="blog_goal_provide_instructions" value="1" class="omissions_check" omission_id="{{$wp_omission->id}}" type="checkbox">  Provide instructions
 						
    		         </div>
					 
 			         <div class="col-md-2">
							
						 <input name="blog_tone_serious_experienced_formal" @if($wp_omission->blog_tone_serious_experienced_formal == 1)checked= 'checked' @endif db_field_name="blog_tone_serious_experienced_formal" value="1" class="omissions_check" omission_id="{{$wp_omission->id}}" type="checkbox">  Serious, experienced, formal
 						
    		         </div>
					 
 			         <div class="col-md-2">
						 
						<input name="blog_tone_funny_witty_satirical" @if($wp_omission->blog_tone_funny_witty_satirical == 1)checked= 'checked' @endif db_field_name="blog_tone_funny_witty_satirical" value="1" class="omissions_check" omission_id="{{$wp_omission->id}}" type="checkbox">  Funny, witty, satirical
						
    		         </div>
					  
    		   </div>
			   <br /><br />
			   
  			  <div class="row">
  			         <div class="col-md-2">
						
						<input name="blog_tone_empathetic_instructional_detailed" @if($wp_omission->blog_tone_empathetic_instructional_detailed == 1)checked= 'checked' @endif db_field_name="blog_tone_empathetic_instructional_detailed" value="1" class="omissions_check" omission_id="{{$wp_omission->id}}" type="checkbox">  Empathetic instructional detailed
  						
     		         </div>
					 
  			         <div class="col-md-2">
						 
						 <input name="blog_tone_casual_conversational_friendly" @if($wp_omission->blog_tone_casual_conversational_friendly == 1)checked= 'checked' @endif db_field_name="blog_tone_casual_conversational_friendly" value="1" class="omissions_check" omission_id="{{$wp_omission->id}}" type="checkbox">  Casual, conversational, friendly
  						
     		         </div>
					 
     		   </div>
			   
    		<br /><br /> 
			   
   			  <div class="row">
   			         <div class="col-md-12">
   						<p>Examples</p>
						
                <textarea rows="7" cols="50" name ="blog_examples" omission_id="{{$wp_omission->id}}" db_field_name="blog_examples"  class ="form-control omissions">{{$wp_omission->blog_examples}}</textarea>
						
      		         </div>
      		   </div>
			   
			   <br /><br />
			   
    			  <div class="row">
    			         <div class="col-md-12">
    						<p>Keywords</p>
    		<textarea rows="7" cols="50" name ="blog_keywords" omission_id="{{$wp_omission->id}}" db_field_name="blog_keywords"  class ="form-control omissions">{{$wp_omission->blog_keywords}}</textarea>
       		         </div>
       		   </div>
			   
			   <br /><br />
				 
  			  <div class="row">
  			         <div class="col-md-12">
  						<p>Guidelines</p>
  		   <textarea rows="7" cols="50" name ="blog_guidelines" omission_id="{{$wp_omission->id}}" db_field_name="blog_guidelines"  class ="form-control omissions">{{$wp_omission->blog_guidelines}}</textarea>
     		         </div>
     		     </div>
			   
			   </div>
			   
			 <br /><br />
			 
			 @include('wp_omissions/_upload_form', array ('section' => 'article','fnumber' => '0', 'array' => $files))
	 
			 <script>
	 
			 $.uploadPreview({
			 	input_field: "#Uploadid0",
			 	preview_box: "#imagen-item-preview0",
			 	no_label: true
			 });
	 
			 $('#Uploadid0').on('change', function () {
			 	$("#imagen-item-preview0").fadeIn("slow");
			 	$('#imagen-item-text0').html($('#Uploadid0').val());		
			 });
	 
			 </script>
	 
			 <br /><br /><br />


@endsection