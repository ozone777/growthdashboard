
@extends('layout')

@section('header')
  
@endsection

@section('content')

<div id ="blog_or_magazine_site">
	
     <br />
	 
	 
	 <div class="row">
			         
	 	<div class="col-md-2">
															
	 		@include('wp_omissions/_web_states')
												
	 	</div>
					
	 </div>
	
	
	<br />

    <div class="row">
		
         <div class="col-md-3">
		    
			
		 </div>
		
           <div class="col-md-2" style="text-align:right;">
				
			   	 <div class="circlep" style="background: #54d4ad;"><a href="/web/{{$wp_omission->id}}">Initial information</a></div>
			  
          </div>
	  
      
             <div class="col-md-2">
			   			   
  			   <div class="circlep" style="background: #9ea6a4;"><a href="/web_development/{{$wp_omission->id}}">Web development</a></div>
  			
            </div>
			
			
        </div>
		
        <div class="row">
               <div class="col-md-6">
			   			   
    			    <h3>{{$product->get_title()}}</h3>
  			
              </div>
          </div>
	  
	 
	
	
	
  <div class="row">
         <div class="col-md-6">
			<p>Site name</p>
	     <input type="text" name="web_design_domain" omission_id="{{$wp_omission->id}}" db_field_name="web_design_domain" value="{{$wp_omission->web_design_domain}}" class = "form-control omissions">
			
         </div>
	 
         <div class="col-md-6">
			<p>Contact form email account</p>
	
			<input type="text" name="web_design_contact_form_email" omission_id="{{$wp_omission->id}}" db_field_name="web_design_contact_form_email" value="{{$wp_omission->web_design_contact_form_email}}" class = "form-control omissions">
	
         </div>
	 
   </div>
   
   <br />
   
   <div class="row">
          <div class="col-md-6">
 			<p>Web site admin username</p>
		
			<input type="text" name="web_design_admin_username" omission_id="{{$wp_omission->id}}" db_field_name="web_design_admin_username" value="{{$wp_omission->web_design_admin_username}}" class = "form-control omissions">
			
		
          </div>
	 
          <div class="col-md-6">
 			<p>Web site admin password <i>(This data will be encrypted)<i></p>
		
				<input type="text" name="web_design_admin_desired_password" omission_id="{{$wp_omission->id}}" db_field_name="web_design_admin_desired_password" value="{{$wp_omission->web_design_admin_desired_password}}" class = "form-control omissions">
		
          </div>
    </div>   
 
     <br />
  
    <div class="row">
	
           <div class="col-md-6">
  			<p>Company address</p>
	
			<textarea rows="7" cols="50" name ="company_address" omission_id="{{$wp_omission->id}}" db_field_name="company_address"  class ="form-control omissions">{{$wp_omission->company_address}}</textarea>
			
	
           </div>
	   
           <div class="col-md-6">
  			<p>Company phones</p>
			
			<textarea rows="7" cols="50" name ="company_phones" omission_id="{{$wp_omission->id}}" db_field_name="company_phones"  class ="form-control omissions">{{$wp_omission->company_phones}}</textarea>
	
           </div>
	   
     </div>
  <br />
	  
	  
      <div class="row">
		
             <div class="col-md-6">
    			<p>Company portfolio</p>
		
				<textarea rows="7" cols="50" name ="company_portfolio" omission_id="{{$wp_omission->id}}" db_field_name="company_portfolio"  class ="form-control omissions">{{$wp_omission->company_portfolio}}</textarea>
		
             </div>
		   
             <div class="col-md-6">
    			<p>5 Web site examples</p>
				
				<textarea rows="7" cols="50" name ="web_design_examples" omission_id="{{$wp_omission->id}}" db_field_name="web_design_examples"  class ="form-control omissions">{{$wp_omission->web_design_examples}}</textarea>
		
             </div>
		   
       </div>
	  <br />
	  
      <div class="row">
		
             <div class="col-md-6">
    			<p>Company domain keys <i>(This data will be encrypted)<i></p>
		
					<textarea rows="7" cols="50" name ="company_domain_keys" omission_id="{{$wp_omission->id}}" db_field_name="company_domain_keys"  class ="form-control omissions">{{$wp_omission->company_domain_keys}}</textarea>
		
             </div>
		   
             <div class="col-md-6">
    			<p>Social networks URL</p>
				
					<textarea rows="7" cols="50" name ="web_design_social_urls" omission_id="{{$wp_omission->id}}" db_field_name="web_design_social_urls"  class ="form-control omissions">{{$wp_omission->web_design_social_urls}}</textarea>
		
             </div>
		   
       </div>
	  <br />
	 

    <br /><br />
   
		<style type="text/css" media="screen">
			
		</style>
		
		<div class="row">
			<div class="col-md-12">
		<p>Logo files</p>
		</div>
		</div>
	
	    
		
		@include('wp_omissions/_upload_form', array ('section' => 'weblogo','fnumber' => '0', 'array' => $logo_files,'updated_view' => 'web'))

		 
	     <br /><br />
		 
		@if($mockup_files[0])
		
		<div class="row">
			<div class="col-md-12">
		<p>Mockup files</p>
		</div>
		</div>
		 
 		<div class="row">
 			
 			 <div class="col-md-12" id="displayImages0">
		
 		  			<div id="gallery" style="display:none;">

			
 		     		 @foreach($mockup_files as $file)
	 	
		     			@if($file->section == "mockup")
					 	
 		     			
   			
			
 		  			<img alt="Preview Image 1"
 		  				 src="/files/{{$wp_omission->id}}/{{$file->file}}.thumb.{{$file->extension}}"
 		  				 data-image="/files/{{$wp_omission->id}}/{{$file->file}}.mockup.{{$file->extension}}"
						
						@if($file->title)
						 
 		  				 data-description="{{$file->title}}">
			
 		     			@else	
						
						data-description="">			

 		     			@endif
						
						@endif
		
		     			
	   
 		     		 @endforeach
		 
 		  		 </div>
						
		
 	   	 	</div>
     	</div>
		
		<br /><br />
		
		@endif
		
		@if(($role == "Contributor") || ($role == "Administrator"))
		
		  @include('wp_omissions/_upload_form', array ('section' => 'mockup','fnumber' => '1', 'array' => $mockup_files, 'updated_view' => 'web'))
		
		@endif
		 
 	     <br /><br />
	 
</div>

@include('wp_omissions/js_logic')

<script>

$( document ).ready(function() {
	
	 omissions_listeners();
	 delete_file_logic();
	 //upload_logic();
	 //delete_file_logic();
	 
	$("#gallery").unitegallery();
	//$("#web_styles_gallery").unitegallery();
	
	
    
});

</script>

@endsection
