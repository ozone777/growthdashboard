@extends('layout')

@section('header')
  
@endsection

@section('content')
    <style>
    	#oboard {
		  	white-space: nowrap;
		    margin-bottom: 10px;
		    overflow-x: auto;
		    overflow-y: hidden;
		    padding-bottom: 10px;
		    position: absolute;
		    top: 61px; right: 0;
		    bottom: 52px; left: 0;
	    }
    	
    	#oboard .ocontainer {
			width: 270px;
			margin: 15px 0 0 10px;
			height: 100%;
			box-sizing: border-box;
			display: inline-block;
			vertical-align: top;
			white-space: nowrap;
    	}
    	
    	#oboard .column {
    		background: #e2e4e6;
    		border-radius: 3px;
    		box-sizing: border-box;
    		display: -webkit-box;
    		display: -webkit-flex;
    		display: -ms-flexbox;
    		display: flex;
    		-webkit-flex-direction: column;
    		-ms-flex-direction: column;
    		flex-direction: column;
    		max-height: 100%;
    		position: relative;
    		white-space: normal;
		}

		.oboard-header {
			-webkit-box-flex: 0;
		    -webkit-flex: 0 0 auto;
		    -ms-flex: 0 0 auto;
		    flex: 0 0 auto;
		    padding: 8px 10px;
		    position: relative;
		    min-height: 18px;
		}
    
    	.oboard-content {
    		-webkit-box-flex: 1;
		    -webkit-flex: 1 1 auto;
		    -ms-flex: 1 1 auto;
		    flex: 1 1 auto;
		    overflow-y: auto;
		    overflow-x: hidden;
		    margin: 0 4px;
		    padding: 0 4px;
		    z-index: 1;
		    min-height: 0;
    	}
    	.oboard-footer {
    		padding: 2px 12px;
    	}
    	#oboard .ocards {
    		margin: 10px 5px 0px;
    		background-color: #fff;
    		padding: 10px;
    		border-radius: 5px;
    		position:relative;
    	}
    	.btnAddCard {
    		cursor: pointer;
			text-align: left;
			border: none;
			background-color: transparent;
    		color: #25a292;
    		font-weight: 600;
    	}

    	.btnAddCard:focus {
    		outline: none;
    	}

    	.btnAddCard:hover {
    		color: #f24;
    	}
    	.ohours {
			background-color: #25a292;
			color: #fff;
			position: absolute;
			bottom: 0;
			right: 0;
			border-radius: 20px 0 5px 0;
			padding: 0 5px 0 20px;
    	}
    </style>

	<div id="oboard">
		<!--
		<div class="ocontainer">
			<div class="column">
				<div class="oboard-header">Seccion 1</div>
				<div class="oboard-content" id="cards">
						<div class="ocards">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						</div>
						<div class="ocards">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						</div>
						<div class="ocards">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						</div>
				</div>
				<div class="oboard-footer">
					<button class="btnAddCard">Add Card</button>
				</div>
			</div>
		</div>
		<div class="ocontainer">
			<div class="column">
				<div class="oboard-header">Seccion 1</div>
				<div class="oboard-content">
						<div class="ocards">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						</div>
						<div class="ocards">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						</div>
				</div>
				<div class="oboard-footer"></div>
			</div>
		</div>
		<div class="ocontainer">
			<div class="column">
				<div class="oboard-header">Seccion 1</div>
				<div class="oboard-content">
						<div class="ocards">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						</div>
						<div class="ocards">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						</div>
						<div class="ocards">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						</div>
						<div class="ocards">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						</div>
						<div class="ocards">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						</div>
				</div>
				<div class="oboard-footer"></div>
			</div>
		</div>
		<div class="ocontainer">
			<div class="column">
				<div class="oboard-header">Seccion 1</div>
				<div class="oboard-content">
						<div class="ocards">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						</div>
						<div class="ocards">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						</div>
						<div class="ocards">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						</div>
						<div class="ocards">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						</div>
				</div>
				<div class="oboard-footer"></div>
			</div>
		</div>
		<div class="ocontainer">
			<div class="column">
				<div class="oboard-header">Seccion 1</div>
				<div class="oboard-content">
						<div class="ocards">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						</div>
				</div>
				<div class="oboard-footer">
					
				</div>
			</div>
		</div>
		-->
	</div>
	
	<script>
		// Datos de ejemplo
		var columns = [
			{
				id: 1,
				'section_name': 'Section_1',
				"cards": [
					{
						"id": 1,
						"hours": 5,
						"title": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod"
					},
					{
						"id": 2,
						"hours": 15,
						"title": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod"
					},
					{
						"id": 3,
						"hours": 3,
						"title": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod"
					},
					{
						"id": 4,
						"hours": 7,
						"title": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod"
					},
					{
						"id": 5,
						"hours": 1,
						"title": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod"
					}
				]
			},
			{
				id: 2,
				'section_name': 'Section_2',
				"cards": [
					{
						"id": 1,
						"hours": 4,
						"title": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod"
					},
					{
						"id": 2,
						"hours": 16,
						"title": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod"
					},
					{
						"id": 3,
						"hours": 21,
						"title": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod"
					},
					{
						"id": 4,
						"hours": 48,
						"title": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod"
					},
					{
						"id": 5,
						"hours": 2,
						"title": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod"
					}
				]
			},
			{
				id: 3,
				'section_name': 'Section_3',
				"cards": [
					{
						"id": 1,
						"hours": 5,
						"title": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod"
					},
					{
						"id": 2,
						"hours": 15,
						"title": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod"
					},
					{
						"id": 3,
						"hours": 3,
						"title": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod"
					},
					{
						"id": 4,
						"hours": 7,
						"title": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod"
					},
					{
						"id": 5,
						"hours": 1,
						"title": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod"
					}
				]
			},
			{
				id: 4,
				'section_name': 'Section_4',
				"cards": [
					{
						"id": 1,
						"hours": 5,
						"title": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod"
					},
					{
						"id": 2,
						"hours": 15,
						"title": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod"
					},
					{
						"id": 3,
						"hours": 3,
						"title": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod"
					},
					{
						"id": 4,
						"hours": 7,
						"title": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod"
					},
					{
						"id": 5,
						"hours": 1,
						"title": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod"
					}
				]
			},
			{
				id: 5,
				'section_name': 'Section_5',
				"cards": [
					{
						"id": 1,
						"hours": 5,
						"title": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod"
					},
					{
						"id": 2,
						"hours": 15,
						"title": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod"
					},
					{
						"id": 3,
						"hours": 3,
						"title": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod"
					},
					{
						"id": 4,
						"hours": 7,
						"title": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod"
					},
					{
						"id": 5,
						"hours": 1,
						"title": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod"
					}
				]
			}
		];
		$(document).ready(function(){
			fillcolumns(columns);
		});

		function fillcolumns(columns){
			var res = '';
			for(i in columns) {
				var section = columns[i];
				res += '<div class="ocontainer">';
				res += '<div class="column">';
				res += '<div class="oboard-header">'+section.section_name+'</div>';
				res += '<div class="oboard-content" id="cards_'+section.id+'">';
				for(o in section.cards) {
					var card = section.cards[o];
					res += '<div class="ocards">';
					res += card.title;
					res += '<div class="ohours">'+card.hours+' Hours</div>';
					res += '</div>';
				}
				res += '</div>';
				res += '<div class="oboard-footer">';
				res += '<button class="btnAddCard" data-target="cards_'+section.id+'">Add Card</button>';
				res += '</div></div></div>';
			}
			$('#oboard').html(res);
		}

		$('#oboard').on('click', '.btnAddCard', function (event) {
			var btn = event.currentTarget;
			var target = $(btn).data('target');
			console.log(event, target);
			$(target).append();
		});
		
	</script>
	
@endsection