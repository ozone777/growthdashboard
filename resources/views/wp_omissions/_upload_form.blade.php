
	    <div class="row">
	           <div class="col-md-12" id="displayImages{{$fnumber}}">
	  			
				<ul id="displayImages{{$fnumber}}" class="displayImages">
					@foreach($array as $file)	 
					
					@if($file->section == $section)
		   				
		   						<li style="position:relative;">
									<a href="/files/{{$wp_omission->id}}/{{$file->file}}{{$file->extension}}" target="_blank">
									<div id="imagen-item-show" 
										style="background-image:url(/files/{{$wp_omission->id}}/{{$file->file}}.thumb.{{$file->extension}});"
										class="imagen-item" 
										tooltip="{{$file->title}}">
									</div>
									</a>
		   							<a href="/delete_file/{{$file->id}}" class="btnDelete" omissions_file_id="{{$file->id}}" omission_id="{{$wp_omission->id}}" form_number="{{$fnumber}}" section ="logo">
										<i class="glyphicon glyphicon-remove"></i>
									</a>
		   						</li>
					@endif
					@endforeach
					<li>
						<div id="imagen-item-preview{{$fnumber}}" class="imagen-item" style="display:none">
							<span>PREVIEW</span>
						</div>
					</li>					
				</ul>
				
				<div id="spinner{{$fnumber}}" class="cssload-loader" style="display:none;position:absolute;top:50%;"></div>
	          </div>
	    </div>
		<br />


<form action="{{ route('wp_omissions_files.store') }}" method="POST" class="form-horizontal frm-logoUpload" enctype="multipart/form-data">
			
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="omission_id" value="{{ $wp_omission->id }}">
			<input type="hidden" name="section" value="{{$section}}">
			<input type="hidden" name="updated_view" value="{{$updated_view}}">
			
	    	<div class="row">		
 	    		<div class="col-md-4">
					<p>Name file</p>
					<input type="text" name="title" value="" class="form-control" id="imagen-item-title" />
				</div>
 	    		<div class="col-md-6">
					<p>&nbsp;</p>
            		<label for="Uploadid{{$fnumber}}" class="form-control" id="imagen-item-text{{$fnumber}}" style="overflow:hidden;"> Click for select logo to upload</label>

					<input type="file" id="Uploadid{{$fnumber}}" name="file" style="display:none" class="Uploadid">
				</div>
				<div class="col-md-2">
					<p>&nbsp;</p>
					<button type="submit" class="form-control button-next btn btn-info addfiles btnSendFileLogo" section="{{$section}}" form_number="{{$fnumber}}">Upload</button>
		   	 	</div>
         	</div>
</form>
