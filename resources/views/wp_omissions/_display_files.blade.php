	    <div class="row">
	           <div class="col-md-12" id="displayImages{{$fnumber}}">
	  			
				<ul id="displayImages{{$fnumber}}" class="displayImages">
					@foreach($array as $file)	 
					
					@if($file->section == $section)
		   				
		   						<li style="position:relative;">
									<a href="/files/{{$wp_omission->id}}/{{$file->file}}{{$file->extension}}" target="_blank">
									<div id="imagen-item-show" 
										style="background-image:url(/files/{{$wp_omission->id}}/{{$file->file}}.thumb.{{$file->extension}});"
										class="imagen-item" 
										tooltip="{{$file->title}}">
									</div>
									</a>
		   							
		   						</li>
					@endif
					@endforeach
					<li>
						<div id="imagen-item-preview{{$fnumber}}" class="imagen-item" style="display:none">
							<span>PREVIEW</span>
						</div>
					</li>					
				</ul>
				
				<div id="spinner{{$fnumber}}" class="cssload-loader" style="display:none;position:absolute;top:50%;"></div>
	          </div>
	    </div>
		<br />