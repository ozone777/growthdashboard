<script>

$.uploadPreview({
	input_field: "#Uploadid0",
	preview_box: "#imagen-item-preview0",
	no_label: true
});

$.uploadPreview({
	input_field: "#Uploadid1",
	preview_box: "#imagen-item-preview1",
	no_label: true
});

$.uploadPreview({
	input_field: "#Uploadid2",
	preview_box: "#imagen-item-preview2",
	no_label: true
});

$.uploadPreview({
	input_field: "#Uploadid3",
	preview_box: "#imagen-item-preview3",
	no_label: true
});

$.uploadPreview({
	input_field: "#Uploadid4",
	preview_box: "#imagen-item-preview4",
	no_label: true
});

$.uploadPreview({
	input_field: "#Uploadid5",
	preview_box: "#imagen-item-preview5",
	no_label: true
});

$.uploadPreview({
	input_field: "#Uploadid6",
	preview_box: "#imagen-item-preview6",
	no_label: true
});

$.uploadPreview({
	input_field: "#Uploadid7",
	preview_box: "#imagen-item-preview7",
	no_label: true
});


$('#Uploadid0').on('change', function () {
	$("#imagen-item-preview0").fadeIn("slow");
	$('#imagen-item-text0').html($('#Uploadid0').val());		
});

$('#Uploadid1').on('change', function () {
	$("#imagen-item-preview1").fadeIn("slow");
	$('#imagen-item-text1').html($('#Uploadid1').val());		
});

$('#Uploadid2').on('change', function () {
	$("#imagen-item-preview2").fadeIn("slow");
	$('#imagen-item-text2').html($('#Uploadid2').val());		
});

$('#Uploadid3').on('change', function () {
	$("#imagen-item-preview3").fadeIn("slow");
	$('#imagen-item-text3').html($('#Uploadid3').val());		
});

$('#Uploadid4').on('change', function () {
	$("#imagen-item-preview4").fadeIn("slow");
	$('#imagen-item-text4').html($('#Uploadid4').val());		
});

$('#Uploadid5').on('change', function () {
	$("#imagen-item-preview5").fadeIn("slow");
	$('#imagen-item-text5').html($('#Uploadid5').val());		
});

$('#Uploadid6').on('change', function () {
	$("#imagen-item-preview6").fadeIn("slow");
	$('#imagen-item-text6').html($('#Uploadid6').val());		
});

$('#Uploadid7').on('change', function () {
	$("#imagen-item-preview7").fadeIn("slow");
	$('#imagen-item-text7').html($('#Uploadid7').val());		
});

$('#Uploadid8').on('change', function () {
	//$("#imagen-item-preview7").fadeIn("slow");
	$('#imagen-item-text8').html($('#Uploadid8').val());		
});

/*
function upload_logic(){






$('.btnSendFileLogo').on('click', function () {
	console.log("form_number");
	console.log($(this).attr("form_number"));
	var form_number_string = $(this).attr("form_number");
	var form_number = parseInt($(this).attr("form_number"));
	var section = $(this).attr("section");
	
	console.log($('.frm-logoUpload')[form_number]);
	
	var oform = new FormData( $('.frm-logoUpload')[form_number] );
	oform.append("_token", "{{csrf_token()}}");
	oform.append("omission_id", "{{$wp_omission->id}}");
	oform.append("section", section);
	oform.append("title", $('#imagen-item-title').val());
	var val = $('#Uploadid'+form_number_string).val().toLowerCase();
	var regex = new RegExp("(.*?)\.(jpg|bmp|png|gif)$");
	if(!(regex.test(val))) {
		oform.append("file_type", 'img');
	} else {
		oform.append("file_type", 'file');
	}
	$.ajax({
		url: '/save_file',
		data: oform,
		dataType: 'json',
		method: 'post',
		cache: false,
		contentType: false,
		processData: false,
		beforeSend: function () {
			$('#displayImages'+form_number_string).css('background', 'rgba(0,0,0,0.5)');
			$('#spinner'+form_number_string).fadeIn('slow');
		},
		fail: function (dat) {
			console.log(dat)
		},
		success: function (dat) {
			console.log(dat)
			var res = '';
			res += '<ul id="displayImages0" class="displayImages">';
			for(d in dat){
				item = dat[d];
				res += '<li style="position:relative;"><a href="/files/{{$wp_omission->id}}/'+item.file+'">';
				res += '<div id="imagen-item-show" class="imagen-item" style="';
				res += 'background-image:url(/files/{{$wp_omission->id}}/'+item.file+')';
				res += '"></div></a>';
				res += '<a href="/delete_file/'+item.id+'" class="btnDelete" omissions_file_id="'+item.id+'" omission_id="{{$wp_omission->id}}" form_number="'+form_number_string+'" section= "'+section+'">';
				res += '<i class="glyphicon glyphicon-remove"></i>';
				res += '</a>';
				res += '</li>';
			}
			
			res += '<li><div id="imagen-item-preview'+form_number_string+'" class="imagen-item" style="display:none;"><span>PREVIEW</span></div></li>'
			res += '</ul>';
			res += '<div id="spinner' + form_number_string + '" class="cssload-loader" style="display:none;position:absolute;top:50%;"></div>';
			
			$('#displayImages'+form_number_string).html(res);
			delete_file_logic();
		},
		complete: function () {
			$('#spinner'+form_number_string).fadeOut('slow');
			$('#displayImages'+form_number_string).css('background', 'transparent');
			
			$.uploadPreview({
				input_field: "#Uploadid"+form_number_string,
				preview_box: "#imagen-item-preview"+form_number_string,
				no_label: true
			});
			
		}
	});
});

}

function delete_file_logic_test(){
  $(".btnDelete").click(function() {
      alert("delete");
  });
}
*/

function delete_file_logic(){
	
    $(".btnDelete").click(function() {
		
  	  console.log($(this).attr("omission_id"));
  	  console.log($(this).attr("omissions_file_id"));
  	  var form_number_string = $(this).attr("form_number");
  	  var form_number = parseInt($(this).attr("form_number"));
	  var section = $(this).attr("section");
	  
        jQuery.ajax({
          url: "/delete_file_ajax",
          type: 'post',
          dataType: 'json',
          data: {omission_id: $(this).attr("omission_id"), omissions_file_id: $(this).attr("omissions_file_id"), section: section, _token: "{{csrf_token()}}"},
			beforeSend: function () {
				$('#displayImages'+form_number_string).css('background', 'rgba(0,0,0,0.5)');
				$('#spinner'+form_number_string).fadeIn('slow');
			},
			fail: function (dat) {
				console.log(dat)
			},
			success: function (dat) {
				console.log(dat)
				var res = '';
				res += '<ul id="displayImages0" class="displayImages">';
				for(d in dat){
					item = dat[d];
					res += '<li style="position:relative;"><a href="/files/{{$wp_omission->id}}/'+item.file+'">';
					res += '<div id="imagen-item-show" class="imagen-item" style="';
					res += 'background-image:url(/files/{{$wp_omission->id}}/'+item.file+')';
					res += '"></div></a>';
					res += '<a href="/delete_file/'+item.id+'" class="btnDelete" omissions_file_id="'+item.id+'" omission_id="{{$wp_omission->id}}" form_number="'+form_number_string+'" section= "'+section+'">';
					res += '<i class="glyphicon glyphicon-remove"></i>';
					res += '</a>';
					res += '</li>';
				}
			
				res += '<li><div id="imagen-item-preview'+form_number_string+'" class="imagen-item" style="display:none;"><span>PREVIEW</span></div></li>'
				res += '</ul>';
				res += '<div id="spinner' + form_number_string + '" class="cssload-loader" style="display:none;position:absolute;top:50%;"></div>';
			
				$('#displayImages'+form_number_string).html(res);
				delete_file_logic();
			},
			complete: function () {
				$('#spinner'+form_number_string).fadeOut('slow');
				$('#displayImages'+form_number_string).css('background', 'transparent');
				
				$.uploadPreview({
					input_field: "#Uploadid"+form_number_string,
					preview_box: "#imagen-item-preview"+form_number_string,
					no_label: true
				});
			}
        });
	  
  	  return false ;
	   
    }); 
	
}




function omissions_listeners(){

 $(".omissions").observe_field(1, function( ) {
   if ($(this).val() != ""){
  
     if ($("#cssload-loader").length == 0){
       //text_field_div.append("<div id='loading_div'></div>");
	   $(this).parent().prepend('<div class="cssload-loader"></div>');
      }
 
     jQuery.ajax({
       url: "/save_omission",
       type: 'post',
       dataType: 'json',
       data: {data : $(this).val(), omission_id: $(this).attr("omission_id"), db_field_name: $(this).attr("db_field_name"),_token: "{{csrf_token()}}"},
  		success: function(data){
			$(".cssload-loader").remove();
		}
     });
   }
 });
 
  $(".omissions_check").click(function() {
	  //alert("hola check");
      
	   
      jQuery.ajax({
        url: "/save_omission",
        type: 'post',
        dataType: 'json',
        data: {data : $(this).val(), omission_id: $(this).attr("omission_id"), db_field_name: $(this).attr("db_field_name")},
   		success: function(data){
   			//alert("success");
			//$(".cssload-loader").remove();
 		}
      });
	  
  });
  
  $(".radio_check").click(function() {
	  
	  
      if ($("#cssload-loader").length == 0){
        //text_field_div.append("<div id='loading_div'></div>");
 	   $(this).parent().prepend('<div class="cssload-loader"></div>');
       }
	  
      jQuery.ajax({
        url: "/save_omission",
        type: 'post',
        dataType: 'json',
        data: {data : $(this).val(), omission_id: $(this).attr("omission_id"), db_field_name: $(this).attr("db_field_name")},
   		success: function(data){
   			//alert("success");
			$(".cssload-loader").remove();
			location.reload();
 		}
      });
  });

} 

</script>

