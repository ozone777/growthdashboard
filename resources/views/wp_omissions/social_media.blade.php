@extends('layout')

@section('header')
  
@endsection

@section('content')

    
 <div class="demo">
   <div id="titleClick-demo">
     <fieldset title="Information">
       <legend></legend>
	   
	   
	   <form action="{{ route('wp_omissions.update', $wp_omission->id) }}" method="POST" id ="omission_form" enctype="multipart/form-data">
	 	<input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" id="omission_id-field" name="omission_id" value="{{$wp_omission->id}}">
	   
	   @include('wp_omissions/states')
	   @include('wp_omissions/general_fields')
 
 	  
	 
	  	<br />
	   
	   
	    	    <div class="row">
	    	           <div class="col-md-12">
	    	  			<p>Company description</p>
	
	  			
	   				{!!Form::textarea('company_description',$wp_omission->company_description,['class' => 'form-control ckeditor_area'])!!}
	
	    	           </div>
	    	     </div>
		 
	  		 <br />
		 
	     	    <div class="row">
	     	           <div class="col-md-12">
	     	  			<p>Corporative values</p>
	
	  			
	    				{!!Form::textarea('company_corporative_values',$wp_omission->company_corporative_values,['class' => 'form-control ckeditor_area'])!!}
	
	     	           </div>
	     	     </div>
		 
		 
	  	   <br />
	   
	  	   <div class="row">
	   	           <div class="col-md-12">
	   	  			<p>Mission</p>
		
 	  			
	  				{!!Form::textarea('company_mission',$wp_omission->company_mission,['class' => 'form-control ckeditor_area'])!!}
		
	   	           </div>
	   	     </div>
		 
	  		 <br />
		 
	    	    <div class="row">
	    	           <div class="col-md-12">
	    	  			<p>Vision</p>
		
 	  			
	   				{!!Form::textarea('company_vision',$wp_omission->company_vision,['class' => 'form-control ckeditor_area'])!!}
		
	    	           </div>
	    	     </div>
		 
		 
	  		 <br />
		 
	     	    <div class="row">
	     	           <div class="col-md-12">
	     	  			<p>Target</p>
		
 	  			
	    				{!!Form::textarea('company_target',$wp_omission->company_target,['class' => 'form-control ckeditor_area'])!!}
		
	     	           </div>
	     	     </div>
		 
	  		 <br />
		 
	      	    <div class="row">
	      	           <div class="col-md-12">
	      	  			<p>Current portfolio</p>
		
 	  			
	     				{!!Form::textarea('company_portfolio',$wp_omission->company_portfolio,['class' => 'form-control ckeditor_area'])!!}
		
	      	           </div>
	      	     </div>
		 
	   		 <br />
		 
	     	    <div class="row">
	     	           <div class="col-md-12">
	     	  			<p>Current promotions</p>
		
 	  			
	    				{!!Form::textarea('company_promos',$wp_omission->company_promos,['class' => 'form-control ckeditor_area'])!!}
		
	     	           </div>
	     	     </div>
		 
	  		 <br />
 

	      	    <div class="row">
	      	           <div class="col-md-12">
	      	  			<p>Social media urls, usernames and passwords</p>
		
 	  			
	     				{!!Form::textarea('social_media_users_and_passwords',$wp_omission->social_media_users_and_passwords,['class' => 'form-control'])!!}
		
	      	           </div>
	      	     </div>
		 
	   		 <br />

	  	     <div class="row">
	  	         <div class="col-md-12">
	                  <button type="submit" id="social_network_general_info" class="form-control button-next  btn btn-info">Save</button>
	  	         </div>
	  	     </div>
		 
	     
		 
	   </form>
	   
	   
 	   
   	   @include('wp_omissions/_upload_form', array ('section' => 'social','fnumber' => '0', 'array' => $files))
	 
   	 <script>
	 
   	 $.uploadPreview({
   	 	input_field: "#Uploadid0",
   	 	preview_box: "#imagen-item-preview0",
   	 	no_label: true
   	 });
	 
   	 $('#Uploadid0').on('change', function () {
   	 	$("#imagen-item-preview0").fadeIn("slow");
   	 	$('#imagen-item-text0').html($('#Uploadid0').val());		
   	 });
	 
   	 </script>
	 
   	 <br /><br /><br />
	   
      
     </fieldset>

     <fieldset title="Create calendar">
       <legend></legend>
	   
	   <form action="{{ route('wp_omissions.update', $wp_omission->id) }}" method="POST" id ="calendar_form" enctype="multipart/form-data">
 		   <input type="hidden" name="_method" value="PUT">
       		<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" id="omission_id-field" name="omission_id" value="{{$wp_omission->id}}">
			
       <div class="row">
		
   	      <div class="col-md-12">
   			  <p>Select social networks</p>
  			  
             </div>
 
		 
   	 </div>
	   
      <div class="row">
		
  	      <div class="col-md-4">
			  <input type="checkbox" id="calendar_facebook" name="calendar_facebook" class="inline_class" value="1"> <p class="inline_class">Facebook</p>
  			  
            </div>
     
		 
             <div class="col-md-4">
				<input type="checkbox" id="calendar_instagram" name="calendar_instagram" class="inline_class" value="1"><p class="inline_class">Instagram</p>
              	
           </div>
		 
             <div class="col-md-4">
				<input type="checkbox" id="calendar_twitter" name="calendar_twitter" class="inline_class" value="1"><p class="inline_class">Twitter</p>
              	
           </div>
		 
  	 </div>
	     

    <div class="row">
		
	      <div class="col-md-4">
			  <p>Days on calendar</p>
			 <select name="calendar_days" id="calendar_days" class="form-control">
			   <option value="">Select the number of days</option>
			   <option value="7" @if($wp_omission->calendar_days == 7) selected @endif>7</option>
			   <option value="15" @if($wp_omission->calendar_days == 15) selected @endif>15</option>
			   <option value="30" @if($wp_omission->calendar_days == 30) selected @endif>30</option>
			   <option value="60" @if($wp_omission->calendar_days == 60) selected @endif>60</option>
			 </select>
          </div>
     
		 
           <div class="col-md-4">
			   <p>Number of daily publications</p>
   			 <select name="calendar_number_of_publications" id="calendar_number_of_publications" class="form-control">
			   <option value="">Select the number of publications</option>
   			   <option value="1">1</option>
   			   <option value="2">2</option>
   			   <option value="3">3</option>
   			   <option value="4">4</option>
   			   <option value="5">5</option>
   			   <option value="6">6</option>
   			 </select>
         </div>
		 
	       <div class="col-md-4">
			   <p>Where date calendar begins</p>
	        	<input class="form-control" name="calendar_date_begins" id = "calendar_date_begins" type="text" value="" required>
	     </div>
		 
	 </div>
	 
	 <div class="row">
	 
       <div class="col-md-4">
		   
		   <input class="form-control times" name="calendar_time_template_1" id = "calendar_time_template_1" type="text" value="{{$wp_omission->calendar_time_template_1}}" autocomplete="off" style="display: none" placeholder="Time template 1" required>
     </div>
	 
       <div class="col-md-4">
		   
        	<input class="form-control times" name="calendar_time_template_2" id = "calendar_time_template_2" type="text" autocomplete="off" style="display: none" placeholder="Time template 2" value="{{$wp_omission->calendar_time_template_2}}" required>
     </div>
	 
       <div class="col-md-4">
		   
        	<input class="form-control times" name="calendar_time_template_3" id = "calendar_time_template_3" type="text" autocomplete="off" style="display: none" placeholder="Time template 3" value="{{$wp_omission->calendar_time_template_3}}" required>
     </div>
	 
	 </div>
	 
	 
	 <div class="row">
	 
       <div class="col-md-4">
		  
        	<input class="form-control times" name="calendar_time_template_4" id = "calendar_time_template_4" type="text" autocomplete="off" style="display: none" placeholder="Time template 4" value="{{$wp_omission->calendar_time_template_4}}" required>
     </div>
	 
       <div class="col-md-4">
		   
        	<input class="form-control times" name="calendar_time_template_5" id = "calendar_time_template_5" type="text" autocomplete="off" style="display: none" placeholder="Time template 5" value="{{$wp_omission->calendar_time_template_5}}" required>
     </div>
	 
       <div class="col-md-4">
		   
        	<input class="form-control times" name="calendar_time_template_6" id = "calendar_time_template_6" type="text" autocomplete="off" style="display: none" placeholder="Time template 6" value="{{$wp_omission->calendar_time_template_6}}" required>
     </div>
	 
	 </div>
	  
     <div class="row">
         <div class="col-md-12">
             <button type="submit" class="form-control button-next  btn btn-info">Save</button>
         </div>
     </div>

       </form>
	   
     </fieldset>
	 
	 
      <fieldset title="Facebook">
		  
        <legend>calendar</legend>
		
	    <div class="row">
		
		      <div class="col-md-6">
				  <p>Time interval</p>
				 <select name="time_interval" id="time_interval_twitter" class="form-control time_interval_logic" social_network = "facebook">
				   <option value="current_week">Current week</option>
				    <option value="next_week">Next week</option>
					<option value="last_week">Last week</option>
				   <option value="current_month">Current month</option>
				   <option value="next_month">Next month</option>
				   <option value="last_month">Last month</option>
				   <option value="January">January</option>
				   <option value="February">February</option>
				   <option value="March">March</option>
				   <option value="April">April</option>
				   <option value="May">May</option>
				   <option value="June">June</option>
				   <option value="July">July</option>
				   <option value="August">August</option>
				   <option value="September">September</option>
				   <option value="October">October</option>
				   <option value="November">November</option>
				   <option value="December">December</option>
				 </select>
	          </div>
		 
		 </div>
		 
		 <div id = "facebook_calendars_area">
		
		<div class="row">
	   	   <div class="col-md-12">
			   
			   
			   
			   
               <table class="table table-condensed table-striped socialscheduling">
                   <thead>
                       <tr>
                           <th>Date</th>
                       <th>Content</th>
   					<th>Imagen</th>
                       
                       </tr>
                   </thead>

                   <tbody>
                       @foreach($wp_omissions_facebook as $key => $calendar)
                           <tr>
                            <td width="20%"><input type="text" name="start_date" class="datetimepicker calendar_fields_logic" calendar_id="{{$calendar->id}}" db_field_name="start_date" value="{{$calendar->start_date}}"></td>
                  			 <td width="50%">
   							 <input type="text" name="link" class="calendar_fields_logic" calendar_id="{{$calendar->id}}" db_field_name="link" value="{{$calendar->link}}" placeholder="Link">
   							 <textarea rows="4" cols="50" class="calendar_fields_logic" calendar_id="{{$calendar->id}}" db_field_name="content"  >{{$calendar->content}}</textarea>
   						 </td>
   						 <td class="uploadtd" width="30%">
							 
   			 				@if($calendar->file)
   			 						<div id="upload-preview{{$key}}" class="imgpreview" style="background-image:url(/ocalendars/{{$calendar->omission_id}}/{{$calendar->file}});"></div>
   			 				@else
							
							<div id="upload-preview{{$key}}" class="imgpreview"></div>
							
							@endif
   							<div id="omlfiledrag">
   							  <center>
   							    <p id="msgupload">
   								    <label for="upload-input{{$key}}" class = "file_title" style="color:#fff;font-size:11.5pt;">Sube archivo aquí</label>
									<form action="/set_calendar_image" method="post" enctype="multipart/form-data" id="frm-upload{{$key}}">
   										<input type="file" id="upload-input{{$key}}" name="ofile_name" class="upload-input" style="display:none;">
										<input type="hidden" name="_token" value="{{csrf_token()}}" />
										<input type="hidden" name="calendar_id" value="{{$calendar->id}}" />
										<input type="hidden" name="" value="{{csrf_token()}}" />
									</form>
								</p>
   							  </center>
   							</div>
							
							 
   						 </td>
                           </tr>
                       @endforeach
                   </tbody>
               </table>
		
       </div>
	   </div>
	   
	    </div>
	   
      </fieldset>
	  
	  
      <fieldset title="Instagram">
         <legend>calendar</legend>
		
		
		
	    <div class="row">
		
		      <div class="col-md-6">
 				  <p>Time interval</p>
 				 <select name="time_interval" id="time_interval_twitter" class="form-control time_interval_logic" social_network = "instagram">
 				   <option value="current_week">Current week</option>
				   <option value="current_month">Current month</option>
 				   <option value="next_week">Next week</option>
 				   <option value="next_month">Next month</option>
 				   <option value="last_week">Last week</option>
 				   <option value="last_month">Last month</option>
 				   <option value="January">January</option>
 				   <option value="February">February</option>
 				   <option value="March">March</option>
 				   <option value="April">April</option>
 				   <option value="May">May</option>
 				   <option value="June">June</option>
 				   <option value="July">July</option>
 				   <option value="August">August</option>
 				   <option value="September">September</option>
 				   <option value="October">October</option>
 				   <option value="November">November</option>
 				   <option value="December">December</option>
 				 </select>
				 
				 <div id="progress">
				 
				  </div>
	          </div>
		 
		 </div>
		 
		 <div id = "instagram_calendars_area">
		
		<div class="row">
	   	   <div class="col-md-12">
			   
			   
			   
			   
               <table class="table table-condensed table-striped socialscheduling">
                   <thead>
                       <tr>
                           <th>Date</th>
                       <th>Content</th>
   					<th>Imagen</th>
                       
                       </tr>
                   </thead>

                   <tbody>
                       @foreach($wp_omissions_instagram as $key => $calendar)
                           <tr>
                            <td width="20%"><input type="text" name="start_date" class="datetimepicker calendar_fields_logic" calendar_id="{{$calendar->id}}" db_field_name="start_date" value="{{$calendar->start_date}}"></td>
                  			 <td width="50%">
   							 <input type="text" name="link" class="calendar_fields_logic" calendar_id="{{$calendar->id}}" db_field_name="link" value="{{$calendar->link}}" placeholder="Link">
   							 <textarea rows="4" cols="50" class="calendar_fields_logic" calendar_id="{{$calendar->id}}" db_field_name="content"  >{{$calendar->content}}</textarea>
   						 </td>
   						 <td class="uploadtd" width="30%">
							 
   			 				@if($calendar->file)
   			 						<div id="upload-previewi{{$key}}" class="imgpreview" style="background-image:url(/ocalendars/{{$calendar->omission_id}}/{{$calendar->file}});"></div>
   			 				@else
							
							<div id="upload-previewo{{$key}}" class="imgpreview"></div>
							
							@endif
   							<div id="omlfiledrag">
   							  <center>
   							    <p id="msgupload">
   								    <label for="upload-inputi{{$key}}" class = "file_title" style="color:#fff;font-size:11.5pt;">Sube archivo aquí</label>
									<form action="/set_calendar_image" method="post" enctype="multipart/form-data" id="frm-upload{{$key}}">
   										<input type="file" id="upload-inputi{{$key}}" name="ofile_name" class="upload-input" style="display:none;">
										<input type="hidden" name="_token" value="{{csrf_token()}}" />
										<input type="hidden" name="calendar_id" value="{{$calendar->id}}" />
										<input type="hidden" name="" value="{{csrf_token()}}" />
									</form>
								</p>
   							  </center>
   							</div>
							
							 
   						 </td>
                           </tr>
                       @endforeach
                   </tbody>
               </table>
		
       </div>
	   </div>
	   
	   </div>
	   
      </fieldset>
	  
	  
      <fieldset title="Twitter">
        <legend>calendar</legend>
		
		
		
	    <div class="row">
		
		      <div class="col-md-6">
				  <p>Time interval</p>
				 <select name="time_interval" id="time_interval_twitter" class="form-control time_interval_logic" social_network = "twitter">
				   <option value="current_week">Current week</option>
				   <option value="current_month">Current month</option>
				   <option value="next_week">Next week</option>
				   <option value="next_month">Next month</option>
				   <option value="last_week">Last week</option>
				   <option value="last_month">Last month</option>
				   <option value="January">January</option>
				   <option value="February">February</option>
				   <option value="March">March</option>
				   <option value="April">April</option>
				   <option value="May">May</option>
				   <option value="June">June</option>
				   <option value="July">July</option>
				   <option value="August">August</option>
				   <option value="September">September</option>
				   <option value="October">October</option>
				   <option value="November">November</option>
				   <option value="December">December</option>
				 </select>
	          </div>
		 
		 </div>
		 
		 <div id = "twitter_calendars_area">
		
		<div class="row">
	   	   <div class="col-md-12">
			   
			   
			   
			   
			   
			   
               <table class="table table-condensed table-striped socialscheduling">
                   <thead>
                       <tr>
                           <th>Date</th>
                       <th>Content</th>
   					<th>Imagen</th>
                       
                       </tr>
                   </thead>

                   <tbody>
                       @foreach($wp_omissions_twitter as $key => $calendar)
                           <tr>
                            <td width="20%"><input type="text" name="start_date" class="datetimepicker calendar_fields_logic" calendar_id="{{$calendar->id}}" db_field_name="start_date" value="{{$calendar->start_date}}"></td>
                  			 <td width="50%">
   							 <input type="text" name="link" class="calendar_fields_logic" calendar_id="{{$calendar->id}}" db_field_name="link" value="{{$calendar->link}}" placeholder="Link">
   							 <textarea rows="4" cols="50" class="calendar_fields_logic" calendar_id="{{$calendar->id}}" db_field_name="content"  >{{$calendar->content}}</textarea>
   						 </td>
   						 <td class="uploadtd" width="30%">
							 
   			 				@if($calendar->file)
   			 						<div id="upload-previewt{{$key}}" class="imgpreview" style="background-image:url(/ocalendars/{{$calendar->omission_id}}/{{$calendar->file}});"></div>
   			 				@else
							
							<div id="upload-previewt{{$key}}" class="imgpreview"></div>
							
							@endif
   							<div id="omlfiledrag">
   							  <center>
   							    <p id="msgupload">
   								    <label for="upload-inputt{{$key}}" class = "file_title" style="color:#fff;font-size:11.5pt;">Sube archivo aquí</label>
									<form action="/set_calendar_image" method="post" enctype="multipart/form-data" id="frm-upload{{$key}}">
   										<input type="file" id="upload-inputt{{$key}}" name="ofile_name" class="upload-input" style="display:none;">
										<input type="hidden" name="_token" value="{{csrf_token()}}" />
										<input type="hidden" name="calendar_id" value="{{$calendar->id}}" />
										<input type="hidden" name="" value="{{csrf_token()}}" />
									</form>
								</p>
   							  </center>
   							</div>
							
							 
   						 </td>
                           </tr>
                       @endforeach
                   </tbody>
               </table>
			   
		
       </div>
	   </div>
	   
	   </div>
	   
      </fieldset>
	  

     <input type="submit" value="Finish!" />
   </div>
 </div>
 
  

  


 
 <script>
 
 function set_calendar_listeners(){
 
	 $(".calendar_fields_logic").observe_field(1, function( ) {
	   if ($(this).val() != ""){
	  
	     //if ($("#loading_div").length == 0){
	     //  text_field_div.append("<div id='loading_div'></div>");
	    // }
  
	     jQuery.ajax({
	       url: "/set_calendar",
	       type: 'get',
	       dataType: 'json',
	       data: {data : $(this).val(), calendar_id: $(this).attr("calendar_id"), db_field_name: $(this).attr("db_field_name")}
	     });
	   }
	 });
 
 } 

 function upload_listeners(){
 	
 
 
$('.uploadtd').each(function () {
	var previewField = $(this).find('.imgpreview')[0];
	var uploadField =  $(this).find('input')[0];
	// console.l∫og("#"+previewField.id, "#"+uploadField.id);
	$.uploadPreview({
	    input_field: "#"+uploadField.id,
	    preview_box: "#"+previewField.id,
	    no_label: true
	  });
	$(uploadField).on('change', function () {
		var oform = new FormData($(this).parent('form')[0]);
		console.log('subiendo archivo '+$(this).val()+' ...');
		$.ajax({
			url: '/set_calendar_image',
			data: oform,
			dataType: 'html',
			method: 'post',
			cache: false,
			contentType: false,
			processData: false,
			fail: function (dat) {
				console.log(dat)
			},
			success: function (dat) {
				console.log(dat)
			}
		})
	});
});

}

 
 $( document ).ready(function() {
		
 	//$('.dates').datetimepicker({step:30});
	
	set_calendar_listeners();
	upload_listeners();
	$('#calendar_date_begins').datetimepicker({
	  timepicker:false,
	  format:'d/m/Y'
	});
	
	$('.times').timepicker();
	
	$("#calendar_form").validate({
		rules: {
			calendar_number_of_publications: "required",
			calendar_days: "required",
		}
	});
	
 
 
 	$('#titleClick-demo').stepy({ titleClick: true });
	
	$("#calendar_number_of_publications").change(function() {
	
		if($(this).val() == "1"){
			console.log("1");
			$(".times").hide();
			$("#calendar_time_template_1").show();
		}else if($(this).val() == "2"){
			$(".times").hide();
			console.log("2");
			$("#calendar_time_template_1").show();
			$("#calendar_time_template_2").show();
		}else if($(this).val() == "3"){
			$(".times").hide();
			console.log("3");
			$("#calendar_time_template_1").show();
			$("#calendar_time_template_2").show();
			$("#calendar_time_template_3").show();
		}else if($(this).val() == "4"){
			$(".times").hide();
			$("#calendar_time_template_1").show();
			$("#calendar_time_template_2").show();
			$("#calendar_time_template_3").show();
			$("#calendar_time_template_4").show();
		}else if($(this).val() == "5"){
			$(".times").hide();
			$("#calendar_time_template_1").show();
			$("#calendar_time_template_2").show();
			$("#calendar_time_template_3").show();
			$("#calendar_time_template_4").show();
			$("#calendar_time_template_5").show();
		}else if($(this).val() == "6"){
			$(".times").hide();
			$("#calendar_time_template_1").show();
			$("#calendar_time_template_2").show();
			$("#calendar_time_template_3").show();
			$("#calendar_time_template_4").show();
			$("#calendar_time_template_5").show();
			$("#calendar_time_template_6").show();
		}
	  
	});
	
	$(".time_interval_logic").change(function() {
	  
		var social_network = $(this).attr("social_network");

		if(social_network == "facebook"){
			
			if($("#loading_div").length == 0){
			    $("#facebook_calendars_area").prepend("<div class='row'><div class='col-md-12'><div id='loading_div'></div></div></div>");
			 }
		
		}else if(social_network == "twitter"){
			
			if($("#loading_div").length == 0){
			    $("#facebook_calendars_area").prepend("<div class='row'><div class='col-md-12'><div id='loading_div'></div></div></div>");
			 }
			
		}else if(social_network == "instagram"){
			
			if($("#loading_div").length == 0){
			    $("#facebook_calendars_area").prepend("<div class='row'><div class='col-md-12'><div id='loading_div'></div></div></div>");
			 }
			 
		}
	  
      jQuery.ajax({
        url: "/get_calendars",
        type: 'get',
        dataType: 'json',
        data: {data : $(this).val(), social_network: $(this).attr("social_network")},
	      success: function(data){
			  	console.log(data["social_network"]);
				console.log(data["calendars"]);
				
				var social_network = data["social_network"];
				var calendars = data["calendars"]
				
				var html_content = "";
				html_content += '<div class="row">';
				html_content += '<div class="col-md-12">';
				html_content += '<table class="table table-condensed table-striped socialscheduling">';
				html_content += '<thead>';
				html_content += '<tr>';
				html_content += '<th>Date</th>';
				html_content += '<th>Content</th>';
				html_content += '<th>Imagen</th>';
				html_content += '</tr>';
				html_content += '</thead>';
				html_content += '<tbody>';
				
				
				for(i in calendars){
					var calendar = calendars[i];
					html_content += '<tr>'
					html_content += '<td width="20%">'
					html_content += '<input type="text" name="start_date" class="datetimepicker calendar_fields_logic" calendar_id="'+calendar.id+'"db_field_name="start_date" value="'+calendar.start_date+'">'
					html_content += '</td>'
					html_content += '<td width="50%">'
					html_content += '<input type="text" name="link" class="calendar_fields_logic" calendar_id="'+calendar.id+'" db_field_name="link" value="'+calendar.link+'" placeholder="Link">';
					html_content += '<textarea rows="4" cols="50" class="calendar_fields_logic" calendar_id="'+calendar.id+'" db_field_name="content">'+calendar.content+'</textarea>';
					html_content += '</td>';
					html_content += '<td class="uploadtd" width="30%">';
					if(calendar.file){
					html_content +=	'<div id="upload-preview'+social_network+i+'" class="imgpreview" ';
					html_content += ' style="background-image:url(/ocalendars/'+calendar.omission_id+'/'+calendar.file+');"></div>'
					}else{
					html_content += '<div id="upload-preview{{$key}}" class="imgpreview"></div>'
					}
					html_content += '<div id="omlfiledrag">';
					html_content += '<center>';
					html_content += '<p id="msgupload">';
					html_content += '<label for="upload-input'+social_network+i+'" class = "file_title" style="color:#fff;font-size:11.5pt;">Sube archivo aquí</label>';
					html_content += '<form action="/set_calendar_image" method="post" enctype="multipart/form-data" id="frm-upload'+social_network+i+'">';
					html_content += '<input type="file" id="upload-input'+social_network+i+'" name="ofile_name" class="upload-input" style="display:none;">';
					html_content += '<input type="hidden" name="_token" value="{{csrf_token()}}" />';
					html_content += '<input type="hidden" name="calendar_id" value="'+calendar.id+'" />';
					html_content += '</form>';
					html_content += '</p>';
					html_content += '</center>';
					html_content += '</div>';	
					
					html_content += '</td>';
					html_content += '</tr>';
				}
				
				html_content += '</tbody>';
				html_content += '</table>';
				html_content += '</div>';
				html_content += '</div>';
				
				if(social_network == "facebook"){
					
					$("#facebook_calendars_area").html(html_content);
				
				}else if(social_network == "twitter"){

					$("#twitter_calendars_area").html(html_content);
					
				}else if(social_network == "instagram"){
					
					$("#instagram_calendars_area").html(html_content);
					
				}
				
				set_calendar_listeners();
				upload_listeners();
	        }
      });
	  
	});
	
	function getRandomArbitrary(min, max) {
	    return Math.random() * (max - min) + min;
	}
	
	$('.datetimepicker').datetimepicker({step:30});
	
	
 });

 </script>
 
 @endsection

 