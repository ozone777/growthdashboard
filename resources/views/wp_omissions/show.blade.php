@extends('layout')
@section('header')
<div class="page-header">
        <h1>Wp_omissions / Show #{{$wp_omission->id}}</h1>
        <form action="{{ route('wp_omissions.destroy', $wp_omission->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('wp_omissions.edit', $wp_omission->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">ID</label>
                    <p class="form-control-static"></p>
                </div>
                <div class="form-group">
                     <label for="otitle">OTITLE</label>
                     <p class="form-control-static">{{$wp_omission->otitle}}</p>
                </div>
                    <div class="form-group">
                     <label for="ofile_name">OFILE_NAME</label>
                     <p class="form-control-static">{{$wp_omission->ofile_name}}</p>
                </div>
                    <div class="form-group">
                     <label for="url">URL</label>
                     <p class="form-control-static">{{$wp_omission->url}}</p>
                </div>
            </form>

            <a class="btn btn-link" href="{{ route('wp_omissions.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

@endsection