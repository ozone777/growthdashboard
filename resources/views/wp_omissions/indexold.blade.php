@extends('layout')

@section('header')
  
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
		
		@foreach($items as $item)
		<div class ="col-md-4 missions">
			
	
			
			<?php
			
			$_product = get_product($item->product_id);
			
			echo('<h4><a href="/set_omission?order_id='. $item->order_id .'&order_item_id='. $item->order_item_id .'&user_id=' . $current_user->ID .'&product_id='. $item->product_id . '">'. get_the_title( icl_object_id( $item->product_id, 'product', false, $current_user->ogrowthlang ) ) . '</a></h4>');
			echo($_product->get_image($size = 'shop_catalog'));
			
				
			?>
			
		</div>	
		
		@endforeach

        </div>
    </div>

@endsection