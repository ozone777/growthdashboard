  <div class="row">
         <div class="col-md-6">
			<p>Company name</p>

			
			<input type="text" name="company_name" omission_id="{{$wp_omission->id}}" db_field_name="company_name" value="{{$wp_omission->company_name}}" class = "form-control omissions">
			
         </div>
	 
         <div class="col-md-6">
			<p>Project manager name</p>
			
			<input type="text" name="project_manager" omission_id="{{$wp_omission->id}}" db_field_name="project_manager" value="{{$wp_omission->project_manager}}" class = "form-control omissions">
			
         </div>
	 
   </div>
   
   <br />
   
   <div class="row">
          <div class="col-md-6">
 			<p>Project manager phone</p>
	
			<input type="text" name="project_manager_phone" omission_id="{{$wp_omission->id}}" db_field_name="project_manager_phone" value="{{$wp_omission->project_manager_phone}}" class = "form-control omissions">
			
	
          </div>
	 
          <div class="col-md-6">
 			<p>Project manager email</p>
	
			<input type="text" name="project_manager_email" omission_id="{{$wp_omission->id}}" db_field_name="project_manager_email" value="{{$wp_omission->project_manager_email}}" class = "form-control omissions">
	
          </div>
	 
    </div>
   
    <br />
	
    <div class="row">
           <div class="col-md-6">
  			<p>Company address</p>
	
			
			<textarea rows="7" cols="50" name ="company_address" omission_id="{{$wp_omission->id}}" db_field_name="company_address"  class ="form-control omissions">{{$wp_omission->company_address}}</textarea>
			
           </div>
	 
           <div class="col-md-6">
  			<p>Company phones</p>
	
			<textarea rows="7" cols="50" name ="company_phones" omission_id="{{$wp_omission->id}}" db_field_name="company_phones"  class ="form-control omissions">{{$wp_omission->company_phones}}</textarea>
	
           </div>
	 
     </div>
   
     <br />
	 
     <div class="row">
            <div class="col-md-6">
   			<p>Company URL</p>
			
			<input type="text" name="company_url" omission_id="{{$wp_omission->id}}" db_field_name="company_url" value="{{$wp_omission->company_url}}" class = "form-control omissions">
            </div>
	 
      </div>
   
      <br />