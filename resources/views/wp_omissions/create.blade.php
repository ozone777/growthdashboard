@extends('layout')

@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-plus"></i> Missions / Create </h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('wp_omissions.store') }}" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                       <label for="name-field">Name</label>
                    <input type="text" id="otitle-field" name="otitle" class="form-control" value="{{ old("name") }}"/>
                       
                 </div>
					
		            <div class="form-group" id="zip_file_url_div">
		              <label for="file_name">Mission image</label>
	                  <input type="file" id="ofile_name-field" name="ofile_name">
		            </div>

                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a class="btn btn-link pull-right" href="{{ route('ogrowthmissions.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection