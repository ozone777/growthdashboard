@extends('layout')

@section('header')
  
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12"  style="text-align:center;" >

	<img src="/planet.gif" height="120" width="120">
	
        </div>
    </div>

  <div class="row">
         <div class="col-md-12">
			<h3>{{$product->get_title()}}</h3>
			
        </div>
    </div>
	
	@include('wp_omissions/general_fields')
	
  	<br /><br /> 
	   
  	  <div class="row">
  	         <div class="col-md-12">
  				<p>Examples</p>
				
           <textarea rows="7" cols="50" name ="logo_examples" omission_id="{{$wp_omission->id}}" db_field_name="logo_examples"  class ="form-control omissions">{{$wp_omission->logo_examples}}</textarea>
				
  	         </div>
  	   </div>
	   
  	   <br /><br />
	   
  		  <div class="row">
  		         <div class="col-md-12">
  					<p>Keywords</p>
  	<textarea rows="7" cols="50" name ="logo_keywords" omission_id="{{$wp_omission->id}}" db_field_name="logo_keywords"  class ="form-control omissions">{{$wp_omission->logo_keywords}}</textarea>
  		         </div>
  		   </div>
	   
  	   <br /><br />
		 
  	  <div class="row">
  	         <div class="col-md-12">
  				<p>Guidelines</p>
     <textarea rows="7" cols="50" name ="logo_guidelines" omission_id="{{$wp_omission->id}}" db_field_name="logo_guidelines"  class ="form-control omissions">{{$wp_omission->logo_guidelines}}</textarea>
  	         </div>
  	     </div>
	   
  	   </div>
	   
  	 <br /><br />
	 
	@if(($role == "Contributor") || ($role == "Administrator"))
	
	  @include('wp_omissions/_upload_form', array ('section' => 'mockup','fnumber' => '0', 'array' => $mockup_files, 'updated_view' => 'logo'))
	
	@else	
	
	  @include('wp_omissions/_display_files', array ('section' => 'mockup','fnumber' => '0', 'array' => $mockup_files, 'updated_view' => 'logo'))			
							
	@endif
	 
	 
	   
	 <br /><br />
	
@endsection