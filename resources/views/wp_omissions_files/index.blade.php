@extends('layout')

@section('header')
    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> Wp_omissions_files
            <a class="btn btn-success pull-right" href="{{ route('wp_omissions_files.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
        </h1>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($wp_omissions_files->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>TITLE</th>
                        <th>FILE</th>
                        <th>SECTION</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($wp_omissions_files as $wp_omissions_file)
                            <tr>
                                <td>{{$wp_omissions_file->id}}</td>
                                <td>{{$wp_omissions_file->title}}</td>
                    <td>{{$wp_omissions_file->file}}</td>
                    <td>{{$wp_omissions_file->section}}</td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('wp_omissions_files.show', $wp_omissions_file->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                    <a class="btn btn-xs btn-warning" href="{{ route('wp_omissions_files.edit', $wp_omissions_file->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    <form action="{{ route('wp_omissions_files.destroy', $wp_omissions_file->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $wp_omissions_files->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection