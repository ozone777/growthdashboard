@extends('layout')

@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-plus"></i> Wp_omissions_files / Create </h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('wp_omissions_files.store') }}" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('title')) has-error @endif">
                       <label for="title-field">Title</label>
                    <input type="text" id="title-field" name="title" class="form-control" value="{{ old("title") }}"/>
                       @if($errors->has("title"))
                        <span class="help-block">{{ $errors->first("title") }}</span>
                       @endif
                    </div>
                  
                    <div class="form-group @if($errors->has('section')) has-error @endif">
                       <label for="section-field">Section</label>
                    <input type="text" id="section-field" name="section" class="form-control" value="{{ old("section") }}"/>
                       @if($errors->has("section"))
                        <span class="help-block">{{ $errors->first("section") }}</span>
                       @endif
                    </div>
					
		            <div class="form-group" id="file">
		              <label for="file_name">Mission image</label>
	                  <input type="file" id="file-field" name="file">
		            </div>
					
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a class="btn btn-link pull-right" href="{{ route('wp_omissions_files.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection