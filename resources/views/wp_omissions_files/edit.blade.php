@extends('layout')

@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> Wp_omissions_files / Edit #{{$wp_omissions_file->id}}</h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('wp_omissions_files.update', $wp_omissions_file->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
				

                <div class="form-group @if($errors->has('title')) has-error @endif">
                       <label for="title-field">Title</label>
                    <input type="text" id="title-field" name="title" class="form-control" value="{{ $wp_omissions_file->title }}"/>
                       @if($errors->has("title"))
                        <span class="help-block">{{ $errors->first("title") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('file')) has-error @endif">
                       <label for="file-field">File</label>
                    <input type="text" id="file-field" name="file" class="form-control" value="{{ $wp_omissions_file->file }}"/>
                       @if($errors->has("file"))
                        <span class="help-block">{{ $errors->first("file") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('section')) has-error @endif">
                       <label for="section-field">Section</label>
                    <input type="text" id="section-field" name="section" class="form-control" value="{{ $wp_omissions_file->section }}"/>
                       @if($errors->has("section"))
                        <span class="help-block">{{ $errors->first("section") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('wp_omissions_files.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection