@extends('layout')

@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-plus"></i> Wp_omissions_styles / Create </h3>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('wp_omissions_styles.store') }}" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('es')) has-error @endif">
                       <label for="es-field">Es</label>
                    <input type="text" id="es-field" name="es" class="form-control" value="{{ old("es") }}"/>
                       @if($errors->has("es"))
                        <span class="help-block">{{ $errors->first("es") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('en')) has-error @endif">
                       <label for="en-field">En</label>
                    <input type="text" id="en-field" name="en" class="form-control" value="{{ old("en") }}"/>
                       @if($errors->has("en"))
                        <span class="help-block">{{ $errors->first("en") }}</span>
                       @endif
                    </div>
                    
                    <div class="form-group">
                       <label for="description-field">Description Español</label>
                    <textarea class="form-control" id="description_es-field" rows="3" name="description_es">{{ old("description_es") }}</textarea>
                  
                    </div>
					
					
                    <div class="form-group">
                       <label for="description-field">Description Ingles</label>
                    <textarea class="form-control" id="description_en-field" rows="3" name="description_en">{{ old("description_en") }}</textarea>
                    </div>
					
		   		    <div class="row">

		    	        <div class="col-md-6">
			
		                     <input type="file" id="file-field" name="file">
		   	            </div>

		   			 </div>
					 
					 <br /><br />
					
				<div class="row">	
                <div class="col-md-12">
                    <button type="submit" class="form-control button-next btn btn-info addfiles">Create</button>
          
                </div>
				</div>
            </form>

        </div>
    </div>
@endsection