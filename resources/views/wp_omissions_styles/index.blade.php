@extends('layout')

@section('header')
    <div class="page-header clearfix">
		
		 <a class="btn btn-success pull-right" href="{{ route('wp_omissions_styles.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
		
        <h3>
           Web Styles
           
        </h3>
		
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($wp_omissions_styles->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>ES</th>
                        <th>EN</th>
                        <th>FILE</th>
                       
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($wp_omissions_styles as $wp_omissions_style)
                            <tr>
                                <td>{{$wp_omissions_style->id}}</td>
                                <td>{{$wp_omissions_style->es}}
								<br />Descripcion:<br /> 
								{{$wp_omissions_style->description_es}}
								</td>
                    <td>{{$wp_omissions_style->en}}
					<br />Description: <br />
					{{$wp_omissions_style->description_en}}
					</td>
                    <td>
					<img src="/styles/{{$wp_omissions_style->file}}" alt="Mountain View" style="width:304px;height:228px;">
					</td>
                    
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('wp_omissions_styles.show', $wp_omissions_style->id) }}">View</a>
                                    <a class="btn btn-xs btn-warning" href="{{ route('wp_omissions_styles.edit', $wp_omissions_style->id) }}">Edit</a>
                                    <form action="{{ route('wp_omissions_styles.destroy', $wp_omissions_style->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $wp_omissions_styles->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection