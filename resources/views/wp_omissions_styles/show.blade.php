@extends('layout')
@section('header')
<div class="page-header">
        <h1>Wp_omissions_styles / Show #{{$wp_omissions_style->id}}</h1>
        <form action="{{ route('wp_omissions_styles.destroy', $wp_omissions_style->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('wp_omissions_styles.edit', $wp_omissions_style->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">ID</label>
                    <p class="form-control-static"></p>
                </div>
                <div class="form-group">
                     <label for="es">ES</label>
                     <p class="form-control-static">{{$wp_omissions_style->es}}</p>
                </div>
                    <div class="form-group">
                     <label for="en">EN</label>
                     <p class="form-control-static">{{$wp_omissions_style->en}}</p>
                </div>
                    <div class="form-group">
                     <label for="file">FILE</label>
                     <p class="form-control-static">{{$wp_omissions_style->file}}</p>
                </div>
                    <div class="form-group">
                     <label for="description">DESCRIPTION</label>
                     <p class="form-control-static">{{$wp_omissions_style->description}}</p>
                </div>
            </form>

            <a class="btn btn-link" href="{{ route('wp_omissions_styles.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

@endsection