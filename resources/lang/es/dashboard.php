<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'my_missions' => 'Mis Misiones',
    'my_account' => 'Mi cuenta',
	'need_help' => 'Ayuda',
	'mission_update' => 'Actualizar Mision',
	'mission_builder' => 'Constructor de misiones',
	'products_and_missions' => 'Productos & Misiones',
	'launch_a_new_mission' => 'Nueva Mision',
	'styles' => 'Estilos Web',

];