<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
	'accepted' => 'Publicación de Blog',
	'infographic' => 'Infografía',
	'social_media_level_one' => 'Redes Sociales Nivel 1',
	'social_media_level_two' => 'Redes Sociales Nivel 2',
	'social_media_level_three' => 'Redes Sociales Nivel 3',
	'online_store' => 'Tienda Online',
	'blog_magazine' => 'Portal Web Revista',
	'portfolio' => 'Sitio Web Portfolio',
    'landing_page' => 'Página De Aterrizaje',
    		

];