<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'my_missions' => 'My missions',
    'my_account' => 'My account',
	'need_help' => 'Need Help',
	'mission_update' => 'Mission Update',
	'mission_builder' => 'Mission Builder',
	'products_and_missions' => 'Products & Missions',
	'launch_a_new_mission' => 'Launch a new mission',
	'styles' => 'Estilos Web',

];