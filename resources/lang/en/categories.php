<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

	'blog_post' => 'Blog Post',
	'infographic' => 'Infographic',
	'social_media_level_one' => 'Social Media Level 1',
	'social_media_level_two' => 'Social Media Level 2',
	'social_media_level_three' => 'Social Media Level 3',
	'online_store' => 'Online Store',
	'blog_magazine' => 'Blog / Magazine',
	'portfolio' => 'Portfolio',
    'landing_page' => 'Landing Page',

];
