<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

	'keywords' => 'Keywords',
	'examples' => 'Examples',
	'guidelines' => 'Guidelines',
	
	'blog_title' => 'Keywords',
	'blog_goal_improve_seo' => 'Improve SEO',
	'blog_goal_become_a_thought_leader' => 'Become a thougth leader',
	'blog_goal_recycle_existing_content' => 'Recycle existing content',
	'blog_goal_provide_instructions' => 'Provide instructions',
	'blog_tone_serious_experienced_formal' => 'Serious experienced formal',
	'blog_tone_funny_witty_satirical' => 'Funny witty satirical',
	'blog_tone_empathetic_instructional_detailed' => 'Emphatetic instructional detailed',
	'blog_tone_casual_conversational_friendly' => 'Casual conversational friendly',
	
	
	'color1' => 'Color 1',
	'color2' => 'Color 2',
	'color3' => 'Color 3',
	'color4' => 'Color 4',
	'color5' => 'Color 5',
	'web_design_admin_email' => 'Admin email',
	'web_design_admin_username' => 'Username',
	'web_design_admin_desired_password' => 'Password',
	'web_design_domain' => 'Domain',
	'web_design_logo' => 'Logo',
	'web_design_section1_title' => 'Section 1',
	'web_design_section2_title' => 'Section 2',
	'web_design_section3_title' => 'Section 3',
	'web_design_section4_title' => 'Section 4',
	'web_design_section5_title' => 'Section 5',
	'web_design_section6_title' => 'Section 6',
	'web_design_section7_title' => 'Section 7',
	'web_design_section1_content' => 'Section 1 content',
	'web_design_section2_content' => 'Section 2 content',
	'web_design_section3_content' => 'Section 3 content',
	'web_design_section4_content' => 'Section 4 content',
	'web_design_section5_content' => 'Section 5 content',
	'web_design_section6_content' => 'Section 6 content',
	'web_design_section7_content' => 'Section 7 content',
	'web_design_contact_form_email' => 'Contact email',
	'web_design_social_urls' => 'Social networks urls',
	'web_design_domain_user_and_password' => 'Domains keys',
	
	
	'store_types_of_products' => 'Products descriptions?',
	'store_product_fields_size' => 'Multiple have sizes?',
	'store_product_fields_color' => 'Multile have colors?',
	'store_product_fields_price' => 'Products have price?',
	'store_product_fields_weight' => 'Products have weight?',
	'store_product_fields_images' => 'Admin have images ?',
	'store_currency' => 'Kind currency',
	'store_delivery_options' => 'Delivery options',
	'store_delivery_areas' => 'Delivery areas',
	'store_location' => 'Logo',
	'store_product_checkout_yes' => 'Online Checkout',
	
	
	'landing_lead_mailchimp' => 'Mailchimb integration?',
	'landing_lead_csv' => 'CSV',
	'landing_lead_email' => 'Email forward',
	'landing_lead_admin_email' => 'Admin email',
	'landing_main_cta' => 'Cta',
	
	'social_media_users_and_passwords' => 'Social media users and passwords',
	'infographic_title' => 'Infographic title',
	'infographic_content' => 'Infographic content',

];
